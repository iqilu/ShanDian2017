package com.iqilu.ksd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.RadioBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class RadioDetailAdapter extends BaseRecyclerAdapter<RadioDetailAdapter.MyViewHolder>{

    private Context context;
    private ArrayList<RadioBean> list;
    private OnItemClickListener onItemClickListener;

    public RadioDetailAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<RadioBean> list){
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_radiodetail, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position, boolean isItem) {
        RadioBean data = list.get(position);
        holder.tvDate.setText(""+data.getDate());
        holder.tvTitle.setText(""+data.getTitle());
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgPlay;
        private TextView tvTitle;
        private TextView tvDate;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                imgPlay = (ImageView) itemView.findViewById(R.id.img_play);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            }

        }
    }
}
