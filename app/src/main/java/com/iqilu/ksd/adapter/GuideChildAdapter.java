package com.iqilu.ksd.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.RadioDetailActivity;
import com.iqilu.ksd.activity.VideoDetailActivity;
import com.iqilu.ksd.bean.GuideChildBean;
import com.iqilu.ksd.event.StopPlayRadioEvent;
import com.iqilu.ksd.event.StopPlayVideoEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

/**
 * 节目单child adapter
 * Created by Coofee on 2016/11/27.
 */

public class GuideChildAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<GuideChildBean> list;
    private String type;

    public GuideChildAdapter(Context context, String type) {
        this.context = context;
        this.type = type;
    }

    public void setData(ArrayList<GuideChildBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final GuideChildBean bean = list.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_guide_child, null);
            holder.imgThumb = (SimpleDraweeView) convertView.findViewById(R.id.img_thumb);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgThumb.setImageURI(Uri.parse(""+bean.getThumb()));
        holder.tvName.setText(""+bean.getCatname());
        holder.tvDate.setText(""+bean.getDate());
        return convertView;
    }

    class ViewHolder {
        SimpleDraweeView imgThumb;
        TextView tvName;
        TextView tvDate;
    }
}
