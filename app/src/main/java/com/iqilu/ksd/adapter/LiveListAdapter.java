package com.iqilu.ksd.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.GalleryActivity;
import com.iqilu.ksd.bean.GalleryItemBean;
import com.iqilu.ksd.bean.LiveImageBean;
import com.iqilu.ksd.bean.LiveItemBean;
import com.iqilu.ksd.bean.NewsBean;
import com.iqilu.ksd.bean.UserBean;
import com.iqilu.ksd.fragment.LiveListFragment;
import com.iqilu.ksd.utils.ConvertUtils;
import com.iqilu.ksd.utils.ScreenUtils;
import com.iqilu.ksd.widget.MyGridView;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/23.
 */

public class LiveListAdapter extends BaseRecyclerAdapter<LiveListAdapter.MyViewHolder>{

    private static final String TAG = "LiveListAdapter";

    int width;
    int height_169;
    int height_43;

    int width_2;
    int height_2;

    int width_3;
    int height_3;

    private static final int TYPE_TEXT = 1;
    private static final int TYPE_PIC = 2;
    private static final int TYPE_VIDEO = 3;
    private static final int TYPE_VOICE = 4;

    private UserBean user;
    private Context context;
    private LiveListFragment fragment;
    private ArrayList<LiveItemBean> list;
    private int voiceId = -1;

    public LiveListAdapter(LiveListFragment fragment,UserBean user) {
        this.fragment = fragment;
        this.user = user;
        this.context = fragment.getContext();
        width = ScreenUtils.getScreenWidth(fragment.getContext()) - ConvertUtils.dp2px(fragment.getContext(), 65);
        height_169 = width / 16 * 9;
        height_43 = width / 4 * 3;

        width_2 = width / 2;
        height_2 = height_43 / 2;

        width_3 = width / 3;
        height_3 = height_43 / 3;
    }

    public void setData(ArrayList<LiveItemBean> list){
        this.list = list;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(fragment.getContext()).inflate(R.layout.list_item_livelist, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        final LiveItemBean item = list.get(position);
        if (!TextUtils.isEmpty(item.getLiverolename())) {
            holder.tvNickname.setText("" + item.getLiverolename());
        } else {
            holder.tvNickname.setText("" + item.getNickname());
        }
        holder.tvDate.setText("" + item.getDate());
        Uri gifUri = Uri.parse("res://"+context.getPackageName()+"/"+R.drawable.voice_play);
        DraweeController gifController = Fresco.newDraweeControllerBuilder().setUri(gifUri).setAutoPlayAnimations(true).build();
        holder.imgVoice.setController(gifController);
        if(voiceId == item.getId()){
            holder.imgVoice.setVisibility(View.VISIBLE);
            holder.imgVoiceStop.setVisibility(View.GONE);
        }else{
            holder.imgVoice.setVisibility(View.GONE);
            holder.imgVoiceStop.setVisibility(View.VISIBLE);
        }
        if (user != null && user.getId() == item.getUid()) {
            holder.tvDel.setVisibility(View.VISIBLE);
            holder.tvEdit.setVisibility(View.VISIBLE);
        } else {
            holder.tvDel.setVisibility(View.GONE);
            holder.tvEdit.setVisibility(View.GONE);
        }
        holder.tvDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.toDelItem(position);
            }
        });
        holder.tvEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.toEditItem(position);
            }
        });
//            holder.txtDescription.setVisibility(View.GONE);
        holder.imgThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (item.getType() == TYPE_PIC) {
                    intent = new Intent(context, GalleryActivity.class);
                    NewsBean bean = new NewsBean(); // 构建一个新的newsbean
                    ArrayList<GalleryItemBean> gallery = new ArrayList<GalleryItemBean>();
                    GalleryItemBean galleryItem = new GalleryItemBean();
                    galleryItem.setImg(item.getBody());
                    gallery.add(galleryItem);
                    bean.setGallery(gallery);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("news", bean);
                    intent.putExtras(bundle);
                    intent.putExtra("position", 0);
                    intent.putExtra("fromLive", true);
                } else if (item.getType() == TYPE_VIDEO) {
                    fragment.toPlayVideo(item.getBody());
                }
                if (intent != null) {
                    context.startActivity(intent);
                }
            }
        });
        holder.layVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(voiceId == item.getId()){
                    fragment.stopPlayVoice();
                }else {
                    fragment.toPlayVoice(item.getBody());
                    voiceId = item.getId();
                }
            }
        });
        switch (item.getType()) {
            case TYPE_PIC: //
                switch (item.getImgs().size()) {
                    case 1:
                        int gWidth = 0;
                        int gHeight = 0;
                        if (!TextUtils.isEmpty(item.getImgs().get(0).getWidth()) && !TextUtils.isEmpty(item.getImgs().get(0).getHeight())) {
                            gWidth = Integer.valueOf(item.getImgs().get(0).getWidth()).intValue();
                            gHeight = Integer.valueOf(item.getImgs().get(0).getHeight()).intValue();
                            gHeight = gHeight * width / gWidth;
                            gWidth = width;
                        } else {
                            gWidth = width;
                            gHeight = height_43;
                        }
//                            holder.gridImg.setNumColumns(1);
//                            holder.gridImg.setAdapter(new MyGridViewAdapter(width, height_43, item.getImgs()));
                        ViewGroup.LayoutParams mParams = holder.imgThumb.getLayoutParams();
                        mParams.width = gWidth;
                        mParams.height = gHeight;
                        holder.imgThumb.setLayoutParams(mParams);
                        holder.imgThumb.setImageURI(Uri.parse(item.getImgs().get(0).getUrl()));
                        holder.btPlay.setVisibility(View.GONE);
                        holder.gvImg.setVisibility(View.GONE);
                        break;
                    case 2:
                        holder.gvImg.setNumColumns(2);
                        holder.gvImg.setAdapter(new MyGridViewAdapter(width_2, height_2, item.getImgs()));
                        break;
                    case 3:
                        holder.gvImg.setNumColumns(3);
                        holder.gvImg.setAdapter(new MyGridViewAdapter(width_3, height_3, item.getImgs()));
                        break;
                    case 4:
                        holder.gvImg.setNumColumns(2);
                        holder.gvImg.setAdapter(new MyGridViewAdapter(width_2, height_2, item.getImgs()));
                        break;
                }
                holder.gvImg.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent = new Intent(context, GalleryActivity.class);
                        NewsBean bean = new NewsBean(); // 构建一个新的newsbean
                        ArrayList<GalleryItemBean> gallery = new ArrayList<GalleryItemBean>();
                        for (int i = 0; i < item.getImgs().size(); i++) {
                            GalleryItemBean galleryItem = new GalleryItemBean();
                            galleryItem.setImg(item.getImgs().get(i).getUrl());
                            gallery.add(galleryItem);
                        }
                        bean.setGallery(gallery);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("news", bean);
                        intent.putExtras(bundle);
                        intent.putExtra("position", position);
                        intent.putExtra("fromLive", true);
                        context.startActivity(intent);
                    }
                });
                holder.gvImg.setVisibility(View.VISIBLE);
                if (!TextUtils.isEmpty(item.getDescription())) {
                    holder.tvMessage.setText(Uri.decode(item.getDescription()));
                    holder.tvMessage.setVisibility(View.VISIBLE);
                } else {
                    holder.tvMessage.setVisibility(View.GONE);
                }
                holder.layVoice.setVisibility(View.GONE);
                if (item.getImgs().size() == 1) {
                    holder.layVideo.setVisibility(View.VISIBLE);
                    holder.gvImg.setVisibility(View.GONE);
                } else {
                    holder.layVideo.setVisibility(View.GONE);
                    holder.gvImg.setVisibility(View.VISIBLE);
                }
                holder.imgType.setImageResource(R.drawable.live_timeline_picture);
                break;
            case TYPE_VIDEO:
                if (!TextUtils.isEmpty(item.getDescription())) {
                    holder.tvMessage.setText(Uri.decode(item.getDescription()));
                    holder.tvMessage.setVisibility(View.VISIBLE);
                } else {
                    holder.tvMessage.setVisibility(View.GONE);
                }
                holder.layVoice.setVisibility(View.GONE);
                holder.layVideo.setVisibility(View.VISIBLE);
                int width2 = ScreenUtils.getScreenWidth(context) - ConvertUtils.dp2px(context, 65);
                int height2 = width2 * 3 / 4;
                ViewGroup.LayoutParams mParams = holder.imgThumb.getLayoutParams();
                mParams.width = width2;
                mParams.height = height2;
                holder.imgThumb.setLayoutParams(mParams);
                holder.imgThumb.setImageURI(Uri.parse(item.getPoster()));
                holder.btPlay.setVisibility(View.VISIBLE);
                holder.gvImg.setVisibility(View.GONE);
                holder.imgType.setImageResource(R.drawable.live_timeline_video);
                break;
            case TYPE_VOICE:
                if (!TextUtils.isEmpty(item.getDescription())) {
                    holder.tvMessage.setText(Uri.decode(item.getDescription()));
                    holder.tvMessage.setVisibility(View.VISIBLE);
                } else {
                    holder.tvMessage.setVisibility(View.GONE);
                }
                holder.layVoice.setVisibility(View.VISIBLE);
                holder.layVideo.setVisibility(View.GONE);
                holder.tvDuration.setText("" + item.getDuration() + "″");
                holder.gvImg.setVisibility(View.GONE);
                holder.imgType.setImageResource(R.drawable.live_timeline_voice);
                break;
            default:
                holder.tvMessage.setText("" + Uri.decode(item.getBody()));
                holder.tvMessage.setVisibility(View.VISIBLE);
                holder.layVoice.setVisibility(View.GONE);
                holder.layVideo.setVisibility(View.GONE);
                holder.gvImg.setVisibility(View.GONE);
                holder.imgType.setImageResource(R.drawable.live_timeline_text);
                break;
        }
        if (position == 0) {
            holder.imgType.setImageResource(R.drawable.live_timeline_begin);
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView imgThumb;
        private SimpleDraweeView imgVoice;
        private ImageView imgVoiceStop;
        private TextView tvNickname;
        private TextView tvDate;
        private TextView tvDel;
        private TextView tvEdit;
        private TextView tvMessage;
        private RelativeLayout layVoice;
        private TextView tvDuration;
        private RelativeLayout layVideo;
        private ImageView btPlay;
        private MyGridView gvImg;
        private ImageView imgType;
        private RelativeLayout layManage;
        private RelativeLayout layContent;

        public MyViewHolder(View itemView) {
            super(itemView);
        }
        public MyViewHolder(View itemView, Boolean isItem) {
            super(itemView);
            if(isItem){
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                imgVoice = (SimpleDraweeView) itemView.findViewById(R.id.img_voice);
                imgVoiceStop = (ImageView) itemView.findViewById(R.id.img_voice_stop);
                tvNickname = (TextView) itemView.findViewById(R.id.tv_nickname);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvDel = (TextView) itemView.findViewById(R.id.tv_del);
                tvEdit = (TextView) itemView.findViewById(R.id.tv_edit);
                tvMessage = (TextView) itemView.findViewById(R.id.tv_message);
                layVoice = (RelativeLayout) itemView.findViewById(R.id.lay_voice);
                tvDuration = (TextView) itemView.findViewById(R.id.tv_duration);
                layVideo = (RelativeLayout) itemView.findViewById(R.id.lay_video);
                btPlay = (ImageView) itemView.findViewById(R.id.bt_play);
                gvImg = (MyGridView) itemView.findViewById(R.id.gv_img);
                imgType = (ImageView) itemView.findViewById(R.id.img_type);
                layManage = (RelativeLayout) itemView.findViewById(R.id.lay_manage);
                layContent = (RelativeLayout) itemView.findViewById(R.id.lay_content);
            }
        }

    }

    public void setVoiceId(int voiceId){
        this.voiceId = voiceId;
    }

    class MyGridViewAdapter extends BaseAdapter {

        int width;
        int height;
        ArrayList<LiveImageBean> imgs;

        public MyGridViewAdapter(int width, int height, ArrayList<LiveImageBean> imgs) {
            this.width = width;
            this.height = height;
            this.imgs = imgs;
        }

        @Override
        public int getCount() {
            return imgs.size();
        }

        @Override
        public Object getItem(int position) {
            return imgs.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                SimpleDraweeView imageView = new SimpleDraweeView(context);
                imageView.setBackground(context.getResources().getDrawable(R.drawable.img_loading));
                imageView.setAdjustViewBounds(true);
                imageView.setMinimumWidth(width);
                //            imageView.setMaxWidth(width);
                imageView.setMinimumHeight(height);
                imageView.setMaxHeight(height);
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                convertView = imageView;
            }
            ((SimpleDraweeView) convertView).setImageURI(Uri.parse(imgs.get(position).getUrl()));
            return convertView;
        }

    }
}
