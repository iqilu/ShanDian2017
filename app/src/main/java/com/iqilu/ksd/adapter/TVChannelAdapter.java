package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.TVBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/25.
 */

public class TVChannelAdapter extends BaseAdapter {

    private Context context;
    private String type;
    private ArrayList<TVBean> list;
    private int curId = -1;

    public TVChannelAdapter(Context context,String type) {
        this.context = context;
        this.type = type;
    }

    public void setData(ArrayList<TVBean> list){
        this.list = list;
    }

    public void setCurId(int curId){
        this.curId = curId;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        TVBean bean = list.get(position);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_tvchannel,null);
            holder.btPlay = (ImageView) convertView.findViewById(R.id.bt_play);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvPlaying = (TextView) convertView.findViewById(R.id.tv_playing);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvName.setText(list.get(position).getTitleResorceId());
        String title = context.getString(R.string.tv_live_title_empty);
        if(!TextUtils.isEmpty(bean.getPlaying())){
            title = bean.getPlaying();
        }
        holder.tvPlaying.setText(String.format(context.getString(R.string.tv_live_title),title));
        if(type.equals("tv")){
            if(bean.getId() == curId){
                holder.btPlay.setImageResource(R.drawable.bt_channel_living);
            }else {
                holder.btPlay.setImageResource(R.drawable.bt_tv_channel_play);
            }
        }else {
            if(bean.getId() == curId){
                holder.btPlay.setImageResource(R.drawable.bt_channel_living);
            }else {
                holder.btPlay.setImageResource(R.drawable.bt_radio_channel_play);
            }
        }
        return convertView;
    }

    class ViewHolder{
        ImageView btPlay;
        TextView tvName;
        TextView tvPlaying;
    }
}
