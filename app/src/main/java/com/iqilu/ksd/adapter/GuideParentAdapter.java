package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.GuideBean;
import com.iqilu.ksd.bean.TVBean;

import java.util.ArrayList;

/**
 * 节目单parent adapter
 * Created by Coofee on 2016/11/27.
 */

public class GuideParentAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<GuideBean> list;
    private int curId = 0;

    public GuideParentAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<GuideBean> list) {
        this.list = list;
    }

    public void setCurId(int curId) {
        this.curId = curId;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        GuideBean bean = list.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_guide_parent, null);
            holder.layTitle = (RelativeLayout) convertView.findViewById(R.id.lay_title);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(bean.getCatname());
        if (bean.getCatid() == curId) {
            holder.layTitle.setBackgroundColor(Color.parseColor("#FFFFFF"));
        } else {
            holder.layTitle.setBackgroundColor(Color.parseColor("#ececec"));
        }
        return convertView;
    }

    class ViewHolder {
        RelativeLayout layTitle;
        TextView tvTitle;
    }
}
