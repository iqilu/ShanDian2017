package com.iqilu.ksd.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.iqilu.ksd.fragment.ImageDetailFragment;

import java.util.List;

/**
 * Created by Coofee on 2016/11/8.
 */

public class GalleryAdapter extends FragmentPagerAdapter {

    private Context context;
    private List<String> fileList;

    public GalleryAdapter(FragmentManager fm, List<String> fileList) {
        super(fm);
        this.fileList = fileList;
    }

    @Override
    public Fragment getItem(int position) {
        String url = fileList.get(position);
        return ImageDetailFragment.newInstance(url);
    }

    @Override
    public int getCount() {
        return fileList == null ? 0 : fileList.size();
    }
}
