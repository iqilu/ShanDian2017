package com.iqilu.ksd.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.MyFavoriteActivity;
import com.iqilu.ksd.bean.FavoriteItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class MyFavoriteAdapter extends BaseRecyclerAdapter<MyFavoriteAdapter.MyViewHolder> {

    private MyFavoriteActivity activity;
    private ArrayList<FavoriteItemBean> list;
    private OnItemClickListener onItemClickListener;

    public MyFavoriteAdapter(MyFavoriteActivity activity) {
        this.activity = activity;
    }

    public void setData(ArrayList<FavoriteItemBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_item_myfavorite, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        FavoriteItemBean item = list.get(position);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(item.getThumb()))
                .setAutoPlayAnimations(true)
                .build();
        holder.imgThumb.setController(controller);
        holder.tvTitle.setText("" + item.getTitle());
        holder.tvDate.setText("" + item.getDate());
        holder.btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyFavoriteActivity) activity).del(position);
            }
        });
        addClickListener(holder);
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    private void addClickListener(final RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView imgThumb;
        TextView tvTitle;
        TextView tvDate;
        TextView btDel;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                btDel = (TextView) itemView.findViewById(R.id.bt_del);
            }

        }
    }
}
