package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.ShakeListBean;
import com.iqilu.ksd.utils.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class ShakeListAdapter extends BaseRecyclerAdapter<ShakeListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ShakeListBean> list;
    private OnItemClickListener onItemClickListener;

    private int mWidth;
    private int mHeight;

    public ShakeListAdapter(Context context) {
        this.context = context;
        mWidth = ScreenUtils.getScreenWidth(context);
        mHeight = (mWidth * 9) / 16;
    }

    public void setData(ArrayList<ShakeListBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_shakelist, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position, boolean isItem) {
        ShakeListBean data = list.get(position);
        holder.imgThumb.setImageURI(Uri.parse(""+data.getThumb()));
        holder.tvStatus.setText(""+data.getStatus());
        holder.tvTitle.setText(""+data.getTitle());

        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView imgThumb;
        RelativeLayout layMain;
        TextView tvStatus;
        TextView tvTitle;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                layMain = (RelativeLayout) itemView.findViewById(R.id.lay_main);
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                tvStatus = (TextView) itemView.findViewById(R.id.tv_status);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
                tvTitle.setTypeface(typeFace);

                ViewGroup.LayoutParams params = layMain.getLayoutParams();
                params.height = mHeight;
                layMain.setLayoutParams(params);
            }

        }
    }
}
