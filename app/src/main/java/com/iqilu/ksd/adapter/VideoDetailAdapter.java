package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.VideoDetailBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/21.
 */

public class VideoDetailAdapter extends BaseRecyclerAdapter<VideoDetailAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<VideoDetailBean> list;
    private OnItemClickListener onItemClickListener;

    public VideoDetailAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<VideoDetailBean> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_videodetail, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position, boolean isItem) {
        VideoDetailBean data = list.get(position);
        holder.imgThumb.setImageURI(Uri.parse(data.getThumb()));
        holder.tvTitle.setText(data.getTitle());
        holder.tvDate.setText(data.getDate());
        holder.tvCatname.setText(data.getCatname());
        addClickListener(holder);
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    private void addClickListener(final RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView imgThumb;
        TextView tvTitle;
        TextView tvDate;
        TextView tvCatname;

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        public MyViewHolder(View itemView, Boolean isItem) {
            super(itemView);
            if (isItem) {
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvCatname = (TextView) itemView.findViewById(R.id.tv_catname);
            }
        }
    }
}
