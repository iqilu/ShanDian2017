package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.WinnerBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/7/28.
 */

public class WinnerAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<WinnerBean> list;

    public WinnerAdapter(Context context,ArrayList<WinnerBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        WinnerBean data = list.get(position);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_winner,null);
            holder.tvName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tvPrize = (TextView) convertView.findViewById(R.id.tv_prize);
            holder.tvPhone = (TextView) convertView.findViewById(R.id.tv_phone);
            Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
            holder.tvName.setTypeface(typeFace);
            holder.tvPrize.setTypeface(typeFace);
            holder.tvPhone.setTypeface(typeFace);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvName.setText(data.getName());
        holder.tvPrize.setText(data.getPrize());
        holder.tvPhone.setText(data.getPhone());

        return convertView;
    }

    class ViewHolder{
        TextView tvName;
        TextView tvPrize;
        TextView tvPhone;
    }
}
