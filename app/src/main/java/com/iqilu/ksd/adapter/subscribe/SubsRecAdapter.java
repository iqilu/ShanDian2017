package com.iqilu.ksd.adapter.subscribe;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsItemBean;
import com.iqilu.ksd.constant.NewsType;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/20.
 */

public class SubsRecAdapter extends BaseAdapter {

    private Context context;

    private ArrayList<NewsItemBean> list;

    public SubsRecAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<NewsItemBean> list){
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final NewsItemBean data = list.get(position);
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_news,null);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.imgThumb = (SimpleDraweeView) convertView.findViewById(R.id.img_thumb);
            holder.imgVideo = (ImageView) convertView.findViewById(R.id.img_video);
            holder.tvComment = (TextView) convertView.findViewById(R.id.tv_comment);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tvCatname = (TextView) convertView.findViewById(R.id.tv_catname);
            holder.imgPk = (ImageView) convertView.findViewById(R.id.img_pk);
            holder.imgLove = (ImageView) convertView.findViewById(R.id.img_love);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(""+data.getTitle());
        holder.imgThumb.setImageURI(Uri.parse(""+data.getThumb()));
        if(!TextUtils.isEmpty(data.getVideo())){
            holder.imgVideo.setVisibility(View.VISIBLE);
        }else {
            holder.imgVideo.setVisibility(View.GONE);
        }
        holder.tvComment.setText(""+data.getLikenum());
        holder.tvDate.setText(""+data.getDate());
        holder.tvCatname.setText(""+data.getCatname());
        if (NewsType.CLUE.equals(data.getType())) {
            holder.imgPk.setImageResource(R.drawable.ic_list_clue);
            holder.imgPk.setVisibility(View.VISIBLE);
            holder.imgLove.setVisibility(View.GONE);
            holder.tvComment.setVisibility(View.GONE);
        } else if (NewsType.LIVE.equals(data.getType())) {
            holder.imgPk.setImageResource(R.drawable.ic_list_live);
            holder.imgPk.setVisibility(View.VISIBLE);
            holder.imgLove.setVisibility(View.GONE);
            holder.tvComment.setVisibility(View.GONE);
        } else {
            holder.imgPk.setVisibility(View.GONE);
            holder.imgLove.setVisibility(View.VISIBLE);
            holder.tvComment.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    class ViewHolder{
        TextView tvTitle;
        SimpleDraweeView imgThumb;
        ImageView imgVideo;
        TextView tvDate;
        TextView tvCatname;
        TextView tvComment;
        ImageView imgPk;
        ImageView imgLove;
    }
}
