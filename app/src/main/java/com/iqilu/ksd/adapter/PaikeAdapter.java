package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.PaikeBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class PaikeAdapter extends BaseRecyclerAdapter<PaikeAdapter.MyViewHolder> {

    private Context context;
    private Boolean isMy = false;
    private ArrayList<PaikeBean> list;
    private OnItemClickListener onItemClickListener;
    private OnDelClickListener onDelClickListener;

    public PaikeAdapter(Context context) {
        this.context = context;
    }

    public PaikeAdapter(Context context, Boolean isMy) {
        this.context = context;
        this.isMy = isMy;
    }

    public void setData(ArrayList<PaikeBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_paike, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position, boolean isItem) {
        PaikeBean item = list.get(position);
        DraweeController controller0;
        DraweeController controller1;
        DraweeController controller2;
        holder.tvTitle.setText("" + item.getTitle());
        holder.tvDate.setText("" + item.getDate());
        holder.tvClick.setText(String.format(context.getString(R.string.pk_click), "" + item.getClick()));
        holder.img0.setVisibility(View.INVISIBLE);
        holder.img1.setVisibility(View.INVISIBLE);
        holder.img2.setVisibility(View.INVISIBLE);
        if (item.getImgs() != null && item.getImgs().length > 0) {
            controller0 = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(item.getImgs()[0])).setAutoPlayAnimations(true).build();
            holder.layImg.setVisibility(View.VISIBLE);
//            holder.img0.setImageURI(Uri.parse(item.getImgs()[0]));
            holder.img0.setController(controller0);
            holder.img0.setVisibility(View.VISIBLE);
            if (item.getImgs().length > 1) {
                controller1 = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(item.getImgs()[1])).setAutoPlayAnimations(true).build();
                holder.img1.setController(controller1);
                holder.img1.setVisibility(View.VISIBLE);
            }
            if (item.getImgs().length > 2) {
                controller1 = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(item.getImgs()[1])).setAutoPlayAnimations(true).build();
                controller2 = Fresco.newDraweeControllerBuilder().setUri(Uri.parse(item.getImgs()[2])).setAutoPlayAnimations(true).build();
                holder.img1.setController(controller1);
                holder.img2.setController(controller2);
                holder.img1.setVisibility(View.VISIBLE);
                holder.img2.setVisibility(View.VISIBLE);
            }
        } else {
            holder.layImg.setVisibility(View.GONE);
        }

        if (isMy) {
            switch (item.getStatus()) {
                case 1:
                    holder.imgStatus.setImageResource(R.drawable.cover_adopted);
                    break;
                case 2:
                    holder.imgStatus.setImageResource(R.drawable.cover_not_adopt);
                    break;
                default:
                    holder.imgStatus.setImageResource(R.drawable.cover_handing);
                    break;
            }
            holder.btDel.setVisibility(View.VISIBLE);
        } else {
            holder.imgStatus.setVisibility(View.GONE);
            holder.btDel.setVisibility(View.GONE);
        }

        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
        if (onDelClickListener != null) {
            holder.btDel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDelClickListener.OnDelClickListener(holder.btDel, holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnDelClickListener(OnDelClickListener onDelClickListener) {
        this.onDelClickListener = onDelClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public interface OnDelClickListener {
        void OnDelClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        LinearLayout layImg;
        ImageView imgStatus;
        SimpleDraweeView img0;
        SimpleDraweeView img1;
        SimpleDraweeView img2;
        TextView tvDate;
        TextView tvClick;
        TextView btDel;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                layImg = (LinearLayout) itemView.findViewById(R.id.lay_img);
                imgStatus = (ImageView) itemView.findViewById(R.id.img_status);
                img0 = (SimpleDraweeView) itemView.findViewById(R.id.img_0);
                img1 = (SimpleDraweeView) itemView.findViewById(R.id.img_1);
                img2 = (SimpleDraweeView) itemView.findViewById(R.id.img_2);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvClick = (TextView) itemView.findViewById(R.id.tv_click);
                btDel = (TextView) itemView.findViewById(R.id.bt_del);
            }

        }
    }
}
