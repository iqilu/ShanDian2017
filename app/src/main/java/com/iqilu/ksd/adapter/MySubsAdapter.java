package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.SubsItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/16.
 */

public class MySubsAdapter extends BaseRecyclerAdapter<MySubsAdapter.ViewHolder> {

    private Context context;
    private boolean isMy = false;
    private ArrayList<SubsItemBean> list;
    private OnItemClickListener onItemClickListener;
    private OnSubscribleListener onSubscribleListener;

    public MySubsAdapter(Context context) {
        this.context = context;
    }

    public MySubsAdapter(Context context, boolean isMy) {
        this.context = context;
        this.isMy = isMy;
    }

    @Override
    public ViewHolder getViewHolder(View view) {
        return new ViewHolder(view, false);
    }

    public void setData(ArrayList<SubsItemBean> list){
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_mysubs, parent, false);
        ViewHolder holder = new ViewHolder(view,true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position, boolean isItem) {
        SubsItemBean data = list.get(position);
        holder.imgThumb.setImageURI(Uri.parse(""+data.getThumb()));
        holder.tvTitle.setText(""+data.getCatname());
        if(data.getSubscribed() == 1){
            holder.btSubs.setImageResource(R.drawable.bt_subscribed);
        }else {
            holder.btSubs.setImageResource(R.drawable.bt_subscribe);
        }
        if(isMy){
            holder.btSubs.setVisibility(View.VISIBLE);
        }else {
            holder.btSubs.setVisibility(View.GONE);
        }
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
        if(onSubscribleListener != null){
            holder.btSubs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSubscribleListener.OnSubscribleListener(holder.btSubs,holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void setOnSubscribleListener(OnSubscribleListener onSubscribleListener){
        this.onSubscribleListener = onSubscribleListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public interface OnSubscribleListener{
        void OnSubscribleListener(View view,int position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private SimpleDraweeView imgThumb;
        private TextView tvTitle;
        private ImageView btSubs;

        public ViewHolder(View itemView) {
            super(itemView);
        }

        public ViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                btSubs = (ImageView) itemView.findViewById(R.id.bt_subs);
            }
        }
    }
}
