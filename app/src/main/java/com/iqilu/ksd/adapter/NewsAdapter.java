package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsItemBean;
import com.iqilu.ksd.constant.NewsItemType;
import com.iqilu.ksd.constant.NewsType;
import com.iqilu.ksd.utils.ConvertUtils;
import com.iqilu.ksd.utils.ScreenUtils;
import com.iqilu.ksd.view.MyVideoView;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Coofee on 2016/11/9.
 */

public class NewsAdapter extends BaseRecyclerAdapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<NewsItemBean> data;
    private OnItemClickListener onItemClickListener;

//    private HeadAdapter headAdapter;

    private int mWidth;
    private int mHeight;
    private int adHeight;
    private int headHeight;

    public NewsAdapter(Context context) {
        this.context = context;
        this.mWidth = ScreenUtils.getScreenWidth(context) - ConvertUtils.dp2px(context, 20);
        this.mHeight = (mWidth * 9) / 16;
        this.adHeight = (mWidth * 120) / 525;
        this.headHeight = (mWidth * 4) / 3;
    }

    public void setData(ArrayList<NewsItemBean> data) {
        this.data = data;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        int itemType;
        NewsItemBean bean = data.get(position);
        String type = bean.getType();
        if (NewsType.ARTICLE.equals(type) || NewsType.LIVE.equals(type) || NewsType.CLUE.equals(type) || NewsType.URL.equals(type)) {
            itemType = NewsItemType.ARTICLE;
        } else if (NewsType.GALLERY.equals(type) || NewsType.PHOTO.equals(type)) {
            itemType = NewsItemType.BIG_IMAGE;
        } else if (NewsType.AD.equals(type)) {
            itemType = NewsItemType.AD;
        } else if (NewsType.HEAD.equals(type)) {
            itemType = NewsItemType.HEAD;
        } else {
            itemType = NewsItemType.OTHER;
        }
        if (bean.getIsbigimage() == 1) {
            itemType = NewsItemType.BIG_IMAGE;
        }
        return itemType;
    }

    @Override
    public RecyclerView.ViewHolder getViewHolder(View view) {
        return new SimpleAdapterViewHolder(view);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = null;
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case NewsItemType.HEAD:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_head, parent, false);
                holder = new ViewHolderHead(view);
                break;
            case NewsItemType.ARTICLE:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_news, parent, false);
                holder = new ViewHolderNews(view);
                break;
//            case NewsItemType.GALLERY:
//                view = LayoutInflater.from(context).inflate(R.layout.list_item_gallery2, null);
//                //view = LayoutInflater.from(context).inflate(R.layout.list_item_news, parent, false);
//                holder = new ViewHolderGallery(view);
//                break;
            case NewsItemType.AD:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_ad, null);
                holder = new ViewHolderAd(view);
                break;
            case NewsItemType.BIG_IMAGE:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_bigimage, null);
                holder = new ViewHolderBigimage(view);
                break;
            default:
                view = LayoutInflater.from(context).inflate(R.layout.list_item_ad, null);
                holder = new SimpleAdapterViewHolder(view);
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position, boolean isItem) {
        final NewsItemBean item = data.get(position);
        String thumbUrl;
        if (getItemViewType(position) == NewsItemType.BIG_IMAGE) {
            thumbUrl = item.getBigimage();
        } else {
            thumbUrl = item.getThumb();
        }
        thumbUrl = TextUtils.isEmpty(thumbUrl) ? "" : thumbUrl;
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(thumbUrl))
                .setAutoPlayAnimations(true)
                .build();
        switch (getItemViewType(position)) {
            case NewsItemType.HEAD:
                ViewHolderHead holderHead = (ViewHolderHead) holder;
                holderHead.headAdapter = new HeadAdapter(context, item.getBannerList());
                holderHead.vpBanner.setAdapter(holderHead.headAdapter);
                holderHead.tabIndicator.setupWithViewPager(holderHead.vpBanner);
                break;
            case NewsItemType.ARTICLE:
                ViewHolderNews holderNews = (ViewHolderNews) holder;
                holderNews.tvTitle.setText(item.getTitle());
                holderNews.imgThumb.setController(controller);
                holderNews.tvDate.setText(item.getDate());
                holderNews.tvCatname.setText(item.getCatname());
                holderNews.tvComment.setText("" + item.getLikenum());
                if (!TextUtils.isEmpty(item.getVideo())) {
                    holderNews.imgVideo.setVisibility(View.VISIBLE);
                } else {
                    holderNews.imgVideo.setVisibility(View.GONE);
                }
                if (NewsType.CLUE.equals(item.getType())) {
                    holderNews.imgPk.setImageResource(R.drawable.ic_list_clue);
                    holderNews.imgPk.setVisibility(View.VISIBLE);
                    holderNews.imgLove.setVisibility(View.GONE);
                    holderNews.tvComment.setVisibility(View.GONE);
                } else if (NewsType.LIVE.equals(item.getType())) {
                    holderNews.imgPk.setImageResource(R.drawable.ic_list_live);
                    holderNews.imgPk.setVisibility(View.VISIBLE);
                    holderNews.imgLove.setVisibility(View.GONE);
                    holderNews.tvComment.setVisibility(View.GONE);
                } else {
                    holderNews.imgPk.setVisibility(View.GONE);
                    holderNews.imgLove.setVisibility(View.VISIBLE);
                    holderNews.tvComment.setVisibility(View.VISIBLE);
                }
                break;
//            case NewsItemType.GALLERY:
//                ViewHolderGallery holderGallery = (ViewHolderGallery) holder;
//                holderGallery.tvTitle.setText(item.getTitle());
//                holderGallery.imgThumb.setController(controller);
//                break;
            case NewsItemType.AD:
                ViewHolderAd holderAd = (ViewHolderAd) holder;
                holderAd.imgAd.setController(controller);
                break;
            case NewsItemType.BIG_IMAGE:
                ViewHolderBigimage holderBigimage = (ViewHolderBigimage) holder;
                holderBigimage.tvTitle.setText(item.getTitle());
                holderBigimage.imgThumb.setController(controller);
                holderBigimage.tvDate.setText("" + item.getDate());
                holderBigimage.tvCatname.setText("" + item.getCatname());
                holderBigimage.tvLove.setText("" + item.getLikenum());
                if (NewsType.ARTICLE.equals(item.getType()) && !TextUtils.isEmpty(item.getVideo())) {
                    holderBigimage.btPlay.setVisibility(View.VISIBLE);
                } else {
                    holderBigimage.btPlay.setVisibility(View.GONE);
                }
                break;
            default:
                SimpleAdapterViewHolder simpleAdapterViewHolder = (SimpleAdapterViewHolder) holder;
                break;
        }
        addClickListener(holder);
    }

    @Override
    public int getAdapterItemCount() {
        return data == null ? 0 : data.size();
    }

    private void addClickListener(final RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    class SimpleAdapterViewHolder extends RecyclerView.ViewHolder {

        public SimpleAdapterViewHolder(View itemView) {
            super(itemView);
        }
    }

    class ViewHolderHead extends RecyclerView.ViewHolder {
        HeadAdapter headAdapter;
        RelativeLayout layThumb;
        ViewPager vpBanner;
        TabLayout tabIndicator;

        public ViewHolderHead(View itemView) {
            super(itemView);
            layThumb = (RelativeLayout) itemView.findViewById(R.id.lay_thumb);
            vpBanner = (ViewPager) itemView.findViewById(R.id.vp_banner);
            tabIndicator = (TabLayout) itemView.findViewById(R.id.tab_indicator);
            ViewGroup.LayoutParams params = layThumb.getLayoutParams();
            params.height = headHeight;
            layThumb.setLayoutParams(params);
        }
    }

    class ViewHolderNews extends RecyclerView.ViewHolder {
        RelativeLayout layThumb;
        TextView tvTitle;
        SimpleDraweeView imgThumb;
        ImageView imgVideo;
        TextView tvDate;
        TextView tvCatname;
        TextView tvComment;
        ImageView imgPk;
        ImageView imgLove;

        public ViewHolderNews(View itemView) {
            super(itemView);
            layThumb = (RelativeLayout) itemView.findViewById(R.id.lay_thumb);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
            imgVideo = (ImageView) itemView.findViewById(R.id.img_video);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvCatname = (TextView) itemView.findViewById(R.id.tv_catname);
            tvComment = (TextView) itemView.findViewById(R.id.tv_comment);
            imgPk = (ImageView) itemView.findViewById(R.id.img_pk);
            imgLove = (ImageView) itemView.findViewById(R.id.img_love);

//            ViewGroup.LayoutParams params = layThumb.getLayoutParams();
//            params.height = headHeight;
//            layThumb.setLayoutParams(params);
        }
    }

//    class ViewHolderGallery extends RecyclerView.ViewHolder {
//        TextView tvTitle;
//        SimpleDraweeView imgThumb;
//        ImageView btPlay;
//
//        public ViewHolderGallery(View itemView) {
//            super(itemView);
//            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
//            imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
//            btPlay = (ImageView) itemView.findViewById(R.id.bt_play);
//        }
//    }

    class ViewHolderAd extends RecyclerView.ViewHolder {
        SimpleDraweeView imgAd;
        RelativeLayout layAD;

        public ViewHolderAd(View itemView) {
            super(itemView);
            layAD = (RelativeLayout) itemView.findViewById(R.id.lay_ad);
            imgAd = (SimpleDraweeView) itemView.findViewById(R.id.img_ad);
            ViewGroup.LayoutParams params = layAD.getLayoutParams();
            params.height = adHeight;
            layAD.setLayoutParams(params);
        }
    }

    class ViewHolderBigimage extends RecyclerView.ViewHolder {
        TextView tvTitle;
        SimpleDraweeView imgThumb;
        ImageView btPlay;
        RelativeLayout layThumb;
        TextView tvLove;
        TextView tvDate;
        TextView tvCatname;

        public ViewHolderBigimage(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
            btPlay = (ImageView) itemView.findViewById(R.id.bt_play);
            tvLove = (TextView) itemView.findViewById(R.id.tv_love);
            tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            tvCatname = (TextView) itemView.findViewById(R.id.tv_catname);
            layThumb = (RelativeLayout) itemView.findViewById(R.id.lay_thumb);
            ViewGroup.LayoutParams params = layThumb.getLayoutParams();
            params.height = mHeight;
            layThumb.setLayoutParams(params);
        }
    }
}
