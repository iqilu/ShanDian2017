package com.iqilu.ksd.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.MyMessageActivity;
import com.iqilu.ksd.bean.MessageBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class MyMessageAdapter extends BaseRecyclerAdapter<MyMessageAdapter.MyViewHolder> {

    private MyMessageActivity activity;
    private ArrayList<MessageBean> list;

    public MyMessageAdapter(MyMessageActivity activity) {
        this.activity = activity;
    }

    public void setData(ArrayList<MessageBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(activity).inflate(R.layout.list_item_mymessage, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        MessageBean item = list.get(position);
        holder.tvTitle.setText("" + item.getMsg());
        holder.tvDate.setText("" + item.getDate());
        holder.btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyMessageActivity) activity).del(position);
            }
        });
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDate;
        TextView btDel;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                btDel = (TextView) itemView.findViewById(R.id.bt_del);
            }

        }
    }
}
