package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.LiveBean;
import com.iqilu.ksd.bean.VoteSignBean;
import com.iqilu.ksd.constant.VoteSign;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/3/22.
 */

public class VoteSignAdapter extends BaseRecyclerAdapter<VoteSignAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<VoteSignBean> list;
    private OnItemClickListener onItemClickListener;

    public VoteSignAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<VoteSignBean> list){
        this.list = list;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_votesign, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position, boolean isItem) {
        VoteSignBean data = list.get(position);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(data.getThumb()))
                .setAutoPlayAnimations(true)
                .build();
        holder.imgLitpic.setController(controller);
        holder.tvTitle.setText("" + data.getTitle());

        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }

    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView imgLitpic;
        private TextView tvTitle;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                imgLitpic = (SimpleDraweeView) itemView.findViewById(R.id.img_litpic);
                Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
                tvTitle.setTypeface(typeFace);
            }

        }
    }
}
