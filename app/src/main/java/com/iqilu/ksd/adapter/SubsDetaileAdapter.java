package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.MyMessageActivity;
import com.iqilu.ksd.bean.MessageBean;
import com.iqilu.ksd.bean.NewsBean;
import com.iqilu.ksd.bean.NewsItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class SubsDetaileAdapter extends BaseRecyclerAdapter<SubsDetaileAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<NewsItemBean> list;
    private OnItemClickListener onItemClickListener;

    public SubsDetaileAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<NewsItemBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_news, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        NewsItemBean item = list.get(position);
        holder.tvTitle.setText(item.getTitle());
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse("" + item.getThumb()))
                .setAutoPlayAnimations(true)
                .build();
        holder.imgThumb.setController(controller);
        holder.tvDate.setText(item.getDate());
        holder.tvCatname.setText(item.getCatname());
        holder.tvComment.setText("" + item.getLikenum());
        if (!TextUtils.isEmpty(item.getVideo())) {
            holder.imgVideo.setVisibility(View.VISIBLE);
        } else {
            holder.imgVideo.setVisibility(View.GONE);
        }
        addClickListener(holder);
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    private void addClickListener(final RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        SimpleDraweeView imgThumb;
        ImageView imgVideo;
        TextView tvDate;
        TextView tvCatname;
        TextView tvComment;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                imgVideo = (ImageView) itemView.findViewById(R.id.img_video);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvCatname = (TextView) itemView.findViewById(R.id.tv_catname);
                tvComment = (TextView) itemView.findViewById(R.id.tv_comment);
            }

        }
    }
}
