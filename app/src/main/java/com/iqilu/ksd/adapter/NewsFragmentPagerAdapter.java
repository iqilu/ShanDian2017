package com.iqilu.ksd.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.iqilu.ksd.bean.ColumnItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/18.
 */

public class NewsFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<ColumnItemBean> columnList;
    private ArrayList<Fragment> fragments;
    private FragmentManager fm;

    public NewsFragmentPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments, ArrayList<ColumnItemBean> columnList) {
        super(fm);
        this.fm = fm;
        this.fragments = fragments;
        this.columnList = columnList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }
}
