package com.iqilu.ksd.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.MyMessageActivity;
import com.iqilu.ksd.bean.MessageBean;
import com.iqilu.ksd.bean.NewsItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class SearchAdapter extends BaseRecyclerAdapter<SearchAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<NewsItemBean> list;
    private NewsAdapter.OnItemClickListener onItemClickListener;

    public SearchAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<NewsItemBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_search, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        NewsItemBean item = list.get(position);
        holder.tvTitle.setText("" + item.getTitle());
        holder.tvDate.setText("" + item.getDate());
        addClickListener(holder);
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    private void addClickListener(final RecyclerView.ViewHolder holder) {
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    public void setOnItemClickListener(NewsAdapter.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;
        TextView tvDate;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
            }

        }
    }
}
