package com.iqilu.ksd.adapter.subscribe;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/19.
 */

public class CatesAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NewsBean> list;

    public CatesAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<NewsBean> list){
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_subs_cate,null);
            holder.imgThumb = (SimpleDraweeView) convertView.findViewById(R.id.img_thumb);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgThumb.setImageURI(Uri.parse(""+list.get(position).getThumb()));
        return convertView;
    }

    class ViewHolder{
        SimpleDraweeView imgThumb;
    }
}
