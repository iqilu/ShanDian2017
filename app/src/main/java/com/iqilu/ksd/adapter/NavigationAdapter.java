package com.iqilu.ksd.adapter;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.ColumnItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/18.
 */

public class NavigationAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<ColumnItemBean> list;

    public NavigationAdapter(Context context, ArrayList<ColumnItemBean> list) {
        this.context = context;
        this.list = list;
    }

    public void setData(ArrayList<ColumnItemBean> list){
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_navigation, null);
            holder.imgThumb = (ImageView) convertView.findViewById(R.id.img_thumb);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgThumb.setImageResource(list.get(position).getIcon());
        if (list.get(position).getId() == 66) { //体育
            holder.tvTitle.setText(R.string.nav_tiyu);
        } else if (list.get(position).getId() == 5) { //娱乐
            holder.tvTitle.setText(R.string.nav_yule);
        } else {
            holder.tvTitle.setText(list.get(position).getName());
        }
        return convertView;
    }

    class ViewHolder {
        ImageView imgThumb;
        TextView tvTitle;
    }
}
