package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.MyLiveActivity;
import com.iqilu.ksd.bean.LiveBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/9.
 */

public class LiveAdapter extends BaseRecyclerAdapter<LiveAdapter.MyViewHolder> {

    private Context context;
    private Boolean isMy = false;
    private ArrayList<LiveBean> list;
    private OnItemClickListener onItemClickListener;

    public LiveAdapter(Context context) {
        this.context = context;
    }

    public LiveAdapter(Context context, Boolean isMy) {
        this.context = context;
        this.isMy = isMy;
    }

    public void setData(ArrayList<LiveBean> list) {
        this.list = list;
    }

    @Override
    public int getAdapterItemViewType(int position) {
        return 0;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view, false);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_live_list, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position, boolean isItem) {
        LiveBean data = list.get(position);
        DraweeController controller = Fresco.newDraweeControllerBuilder()
                .setUri(Uri.parse(data.getLitpic()))
                .setAutoPlayAnimations(true)
                .build();
//        holder.imgLitpic.setImageURI(Uri.parse(data.getLitpic()));
        holder.imgLitpic.setController(controller);
        holder.tvTitle.setText("" + data.getTitle());
        if (!TextUtils.isEmpty(data.getLiverolename())) {
            holder.tvName.setText(String.format(context.getString(R.string.live_people), data.getLiverolename()));
        } else {
            holder.tvName.setText(String.format(context.getString(R.string.live_people), data.getNickname()));
        }
        if(data.getIsgroup() == 1){
            holder.layStatus.setVisibility(View.GONE);
            holder.tvGroup.setText(""+data.getGrouptitle());
            holder.tvCount.setText(String.format(context.getString(R.string.live_group_count),""+data.getLivenum()));
            holder.tvGroup.setVisibility(View.VISIBLE);
            holder.tvCount.setVisibility(View.VISIBLE);
        }else {
            holder.layStatus.setVisibility(View.VISIBLE);
            holder.tvGroup.setVisibility(View.GONE);
            holder.tvCount.setVisibility(View.GONE);
        }
        if(data.getLivestatus() == 1){
            holder.imgStatus.setImageResource(R.drawable.live_zhibozhong);
        }else if(data.getLivestatus() == 2){
            holder.imgStatus.setImageResource(R.drawable.live_yugao);
        }else {
            holder.imgStatus.setImageResource(R.drawable.live_huigu);
        }
        if (isMy) {
            holder.layFunction.setVisibility(View.VISIBLE);
        } else {
            holder.layFunction.setVisibility(View.GONE);
        }
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
        holder.btEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyLiveActivity) context).edit(position);
            }
        });
        holder.btDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MyLiveActivity) context).del(position);
            }
        });
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private SimpleDraweeView imgLitpic;
        private TextView tvTitle;
        private TextView tvName;
        private TextView tvGroup;
        private TextView tvCount;
        private ImageView imgStatus;
        private RelativeLayout layStatus;
        private LinearLayout layFunction;
        private ImageView btEdit;
        private ImageView btDel;

        public MyViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                imgLitpic = (SimpleDraweeView) itemView.findViewById(R.id.img_litpic);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                tvName = (TextView) itemView.findViewById(R.id.tv_name);
                tvGroup = (TextView) itemView.findViewById(R.id.tv_group);
                tvCount = (TextView) itemView.findViewById(R.id.tv_count);
                imgStatus = (ImageView) itemView.findViewById(R.id.img_status);
                layStatus = (RelativeLayout) itemView.findViewById(R.id.lay_status);
                layFunction = (LinearLayout) itemView.findViewById(R.id.lay_function);
                btEdit = (ImageView) itemView.findViewById(R.id.bt_edit);
                btDel = (ImageView) itemView.findViewById(R.id.bt_del);

                Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
                tvTitle.setTypeface(typeFace);
                tvGroup.setTypeface(typeFace);
            }

        }
    }
}
