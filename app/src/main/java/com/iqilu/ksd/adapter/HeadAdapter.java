package com.iqilu.ksd.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.ADActivity;
import com.iqilu.ksd.activity.GalleryActivity;
import com.iqilu.ksd.activity.LiveActivity;
import com.iqilu.ksd.activity.NewsDetailActivity;
import com.iqilu.ksd.activity.PaikeActivity;
import com.iqilu.ksd.bean.BannerBean;
import com.iqilu.ksd.constant.NewsType;
import com.iqilu.ksd.utils.CacheUtils;
import com.iqilu.ksd.utils.FileUtils;
import com.iqilu.ksd.view.MyVideoView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/20.
 */

public class HeadAdapter extends PagerAdapter {

    private Context context;
    private ArrayList<BannerBean> list;

    public HeadAdapter(Context context, ArrayList<BannerBean> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_top_head, null);
        final BannerBean data = list.get(position);
        MyVideoView vvPlayer = (MyVideoView) view.findViewById(R.id.vv_player);
        TextView tvTitle = (TextView) view.findViewById(R.id.tv_title);
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
        tvTitle.setTypeface(typeFace);
        final SimpleDraweeView imgThumb = (SimpleDraweeView) view.findViewById(R.id.img_thumb);
        imgThumb.setImageURI(Uri.parse(data.getThumb()));
        tvTitle.setText("" + data.getTitle());

        vvPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                mp.start();
                return true;
            }
        });
        vvPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.start();
                mp.setLooping(true);
            }
        });

        vvPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0f, 0f);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        imgThumb.setVisibility(View.GONE);
                    }
                }, 500);
            }
        });

        Uri uri;
        File file = new File("" + data.getVideourl());
        String local = CacheUtils.getVideoDir(context) + file.getName();
        if (FileUtils.isFileExists(local)) {
            uri = Uri.parse(local);
        } else {
            uri = Uri.parse("" + data.getVideourl());
        }

        vvPlayer.setVideoURI(uri);
        vvPlayer.start();

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (data.getType().equals(NewsType.ARTICLE)) {
                    intent = new Intent(context, NewsDetailActivity.class);
                    intent.putExtra("id", Integer.valueOf("" + data.getCon()));
                    intent.putExtra("catId", 0);
                } else if (data.getType().equals(NewsType.GALLERY) || data.getType().equals(NewsType.PHOTO)) {
                    intent = new Intent(context, GalleryActivity.class);
                    intent.putExtra("id", Integer.valueOf("" + data.getCon()));
                    intent.putExtra("catId", 0);
                    intent.putExtra("position", 0);
                } else if (data.getType().equals(NewsType.AD) || data.getType().equals(NewsType.URL)) {
                    intent = new Intent(context, ADActivity.class);
                    intent.putExtra("title", data.getTitle());
                    intent.putExtra("type", data.getType());
                    intent.putExtra("adUrl", data.getCon());
                    if (data.getType().equals(NewsType.AD)) {
                        intent.putExtra("shareicon", data.getThumb());
                    } else {
                        intent.putExtra("thumb", data.getThumb());
                    }
                } else if (data.getType().equals(NewsType.LIVE)) {
                    intent = new Intent(context, LiveActivity.class);
                    intent.putExtra("id", Integer.valueOf("" + data.getCon()));
                } else if (data.getType().equals(NewsType.CLUE)) {
                    intent = new Intent(context, PaikeActivity.class);
                    intent.putExtra("id", Integer.valueOf("" + data.getCon()));
                }
                if (intent != null) {
                    context.startActivity(intent);
                }
            }
        });

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        super.destroyItem(container, position, object);
        container.removeView((View) object);
    }
}
