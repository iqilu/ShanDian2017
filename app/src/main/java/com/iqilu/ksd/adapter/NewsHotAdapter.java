package com.iqilu.ksd.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/16.
 */

public class NewsHotAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NewsItemBean> list;

    public NewsHotAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<NewsItemBean> list){
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NewsItemBean item = list.get(position);
        ViewHolder holder;
        if(convertView == null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_recommend,null);
            holder.imgDot = (ImageView) convertView.findViewById(R.id.img_dot);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTitle.setText(""+item.getTitle());
        return convertView;
    }

    class ViewHolder{
        ImageView imgDot;
        TextView tvTitle;
    }
}
