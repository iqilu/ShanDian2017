package com.iqilu.ksd.adapter.subscribe;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.iqilu.ksd.bean.SubsItemBean;
import com.iqilu.ksd.fragment.SubsListFragment;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/20.
 */

public class SubsLightningAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private ArrayList<SubsItemBean> list;
    private ArrayList<SubsListFragment> fragments = new ArrayList<SubsListFragment>();

    public SubsLightningAdapter(FragmentManager fm, Context context,ArrayList<SubsItemBean> list) {
        super(fm);
        this.context = context;
        this.list = list;
        for (int i = 0;i<4;i++){
            SubsListFragment fragment = new SubsListFragment();
            Bundle args = new Bundle();
            args.putSerializable("list", list);
            args.putInt("position",i);
            fragment.setArguments(args);
            fragments.add(fragment);
        }
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return 4;
    }
}
