package com.iqilu.ksd.adapter.subscribe;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.SubsDetailActivity;
import com.iqilu.ksd.bean.NewsItemBean;
import com.iqilu.ksd.constant.NewsType;
import com.muzhi.camerasdk.library.utils.T;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/19.
 */

public class SubsDetailAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NewsItemBean> list;
    private Typeface typeFace;

    public SubsDetailAdapter(Context context) {
        this.context = context;
        typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
    }

    public void setData(ArrayList<NewsItemBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        final NewsItemBean data = list.get(position);
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_subs_detail, null);
            holder.layHead = (RelativeLayout) convertView.findViewById(R.id.lay_head);
            holder.imgLogo = (SimpleDraweeView) convertView.findViewById(R.id.img_logo);
            holder.tvClass = (TextView) convertView.findViewById(R.id.tv_class);
            holder.imgInto = (ImageView) convertView.findViewById(R.id.img_into);
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tvCountNew = (TextView) convertView.findViewById(R.id.tv_count_new);
            holder.imgThumb = (SimpleDraweeView) convertView.findViewById(R.id.img_thumb);
            holder.imgVideo = (ImageView) convertView.findViewById(R.id.img_video);
            holder.tvComment = (TextView) convertView.findViewById(R.id.tv_comment);
            holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tvCatname = (TextView) convertView.findViewById(R.id.tv_catname);
            holder.imgPk = (ImageView) convertView.findViewById(R.id.img_pk);
            holder.imgLove = (ImageView) convertView.findViewById(R.id.img_love);
            holder.tvClass.setTypeface(typeFace);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.imgLogo.setImageURI(Uri.parse("" + data.getImgsrc()));
        holder.tvClass.setText("" + data.getCatname());
        holder.tvTitle.setText("" + data.getTitle());
        if (!"0".equals("" + data.getNovisitnum())) {
            holder.tvCountNew.setText("" + data.getNovisitnum());
            holder.tvCountNew.setVisibility(View.VISIBLE);
        } else {
            holder.tvCountNew.setVisibility(View.GONE);
        }
        holder.imgThumb.setImageURI(Uri.parse("" + data.getThumb()));
        if (!TextUtils.isEmpty(data.getVideo())) {
            holder.imgVideo.setVisibility(View.VISIBLE);
        } else {
            holder.imgVideo.setVisibility(View.GONE);
        }
        holder.tvComment.setText("" + data.getLikenum());
        holder.tvDate.setText("" + data.getDate());
        holder.tvCatname.setText("" + data.getCatname());
        if (NewsType.CLUE.equals(data.getType())) {
            holder.imgPk.setImageResource(R.drawable.ic_list_clue);
            holder.imgPk.setVisibility(View.VISIBLE);
            holder.imgLove.setVisibility(View.GONE);
            holder.tvComment.setVisibility(View.GONE);
        } else if (NewsType.LIVE.equals(data.getType())) {
            holder.imgPk.setImageResource(R.drawable.ic_list_live);
            holder.imgPk.setVisibility(View.VISIBLE);
            holder.imgLove.setVisibility(View.GONE);
            holder.tvComment.setVisibility(View.GONE);
        } else {
            holder.imgPk.setVisibility(View.GONE);
            holder.imgLove.setVisibility(View.VISIBLE);
            holder.tvComment.setVisibility(View.VISIBLE);
        }

        holder.layHead.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SubsDetailActivity.class);
                intent.putExtra("catid", data.getCatid());
                context.startActivity(intent);
                data.setNovisitnum("0");
                SubsDetailAdapter.this.notifyDataSetChanged();
            }
        });

        return convertView;
    }

    class ViewHolder {
        RelativeLayout layHead;
        SimpleDraweeView imgLogo;
        TextView tvClass;
        ImageView imgInto;
        TextView tvTitle;
        TextView tvCountNew;
        SimpleDraweeView imgThumb;
        ImageView imgVideo;
        TextView tvComment;
        TextView tvDate;
        TextView tvCatname;
        ImageView imgPk;
        ImageView imgLove;
    }
}
