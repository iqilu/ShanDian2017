package com.iqilu.ksd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.CommentBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/21.
 */

public class CommentAdapter extends BaseRecyclerAdapter<CommentAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<CommentBean> list;

    public CommentAdapter(Context context) {
        this.context = context;
    }

    public void setData(ArrayList<CommentBean> list) {
        this.list = list;
    }

    @Override
    public MyViewHolder getViewHolder(View view) {
        return new MyViewHolder(view);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_comment, parent, false);
        MyViewHolder holder = new MyViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position, boolean isItem) {
        CommentBean data = list.get(position);
        holder.imgAvatar.setImageURI(Uri.parse(data.getAvatar()));
        holder.tvName.setText(data.getNickname());
        holder.tvDate.setText(data.getDate());
        holder.tvComment.setText(Uri.decode(data.getComment()));
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        SimpleDraweeView imgAvatar;
        TextView tvName;
        TextView tvDate;
        TextView tvComment;

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        public MyViewHolder(View itemView, Boolean isItem) {
            super(itemView);
            if (isItem) {
                imgAvatar = (SimpleDraweeView) itemView.findViewById(R.id.img_avatar);
                tvName = (TextView) itemView.findViewById(R.id.tv_name);
                tvDate = (TextView) itemView.findViewById(R.id.tv_date);
                tvComment = (TextView) itemView.findViewById(R.id.tv_comment);
            }
        }
    }
}
