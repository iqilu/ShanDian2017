package com.iqilu.ksd.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.andview.refreshview.recyclerview.BaseRecyclerAdapter;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsItemBean;
import com.iqilu.ksd.constant.NewsType;
import com.iqilu.ksd.utils.ScreenUtils;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/17.
 */

public class SubsAdapter extends BaseRecyclerAdapter<SubsAdapter.SubsViewHolder> {

    private Context context;
    private ArrayList<NewsItemBean> list;
    private OnItemClickListener onItemClickListener;

    private int mWidth;
    private int mHeight;

    public SubsAdapter(Context context) {
        this.context = context;
        mWidth = ScreenUtils.getScreenWidth(context);
        mHeight = (mWidth * 9) / 16;
    }

    public void setData(ArrayList<NewsItemBean> list) {
        this.list = list;
    }

    @Override
    public SubsViewHolder getViewHolder(View view) {
        return new SubsViewHolder(view, false);
    }

    @Override
    public SubsViewHolder onCreateViewHolder(ViewGroup parent, int viewType, boolean isItem) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_subs, parent, false);
        SubsViewHolder holder = new SubsViewHolder(view, true);
        return holder;
    }

    @Override
    public void onBindViewHolder(final SubsViewHolder holder, int position, boolean isItem) {
        NewsItemBean data = list.get(position);
        if (!TextUtils.isEmpty(data.getThumb())) {
            DraweeController controller = Fresco.newDraweeControllerBuilder()
                    .setUri(Uri.parse(data.getThumb()))
                    .setAutoPlayAnimations(true)
                    .build();
//        holder.imgThumb.setImageURI(Uri.parse(""+data.getThumb()));
            holder.imgThumb.setController(controller);
        }
        holder.imgLogo.setImageURI(Uri.parse("" + data.getImgsrc()));
        holder.tvTitle.setText("" + data.getTitle());
        holder.tvCatname.setText("" + data.getCatname());
        if (! TextUtils.isEmpty(data.getVideo())) {
            holder.btPlay.setVisibility(View.VISIBLE);
        } else {
            holder.btPlay.setVisibility(View.GONE);
        }
        if (onItemClickListener != null) {
            holder.itemView.setBackgroundResource(R.drawable.bg_recycler);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.OnItemClickListener(holder.itemView, holder.getLayoutPosition());
                }
            });
        }
    }

    @Override
    public int getAdapterItemCount() {
        return list == null ? 0 : list.size();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }


    public class SubsViewHolder extends RecyclerView.ViewHolder {

        private RelativeLayout layThumb;
        private TextView tvTitle;
        private SimpleDraweeView imgThumb;
        private ImageView btPlay;
        private TextView tvCatname;
        private SimpleDraweeView imgLogo;

        public SubsViewHolder(View itemView) {
            super(itemView);
        }

        public SubsViewHolder(View itemView, boolean isItem) {
            super(itemView);
            if (isItem) {
                layThumb = (RelativeLayout) itemView.findViewById(R.id.lay_thumb);
                tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
                imgThumb = (SimpleDraweeView) itemView.findViewById(R.id.img_thumb);
                tvCatname = (TextView) itemView.findViewById(R.id.tv_catname);
                btPlay = (ImageView) itemView.findViewById(R.id.bt_play);
                imgLogo = (SimpleDraweeView) itemView.findViewById(R.id.img_logo);

                Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
                tvTitle.setTypeface(typeFace);

                ViewGroup.LayoutParams params = layThumb.getLayoutParams();
                params.height = mHeight;
                layThumb.setLayoutParams(params);
            }

        }
    }

}
