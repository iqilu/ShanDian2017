package com.iqilu.ksd.bean;

import java.io.Serializable;

public class PictureBean implements Serializable {

    private static final long serialVersionUID = -714275605062641162L;

    String path;

    String thumbPath;

    long modifiedTime;

    public long getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(long modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }
}
