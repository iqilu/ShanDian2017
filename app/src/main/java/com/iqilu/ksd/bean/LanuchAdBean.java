package com.iqilu.ksd.bean;

/**
 * 启动界面广告
 * Created by coofee on 2015/12/2.
 */
public class LanuchAdBean {

    private String title;

    private String url;

    private String img;

    private String shareicon;

    private String md5;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShareicon() {
        return shareicon;
    }

    public void setShareicon(String shareicon) {
        this.shareicon = shareicon;
    }
}
