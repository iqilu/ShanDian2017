package com.iqilu.ksd.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by coofee on 2015/11/6.
 */
public class NewsBean implements Serializable {

    private int id;

    private int catid;

    private String title;

    private String date;

    private String copyfrom;

    private String video;

    private String poster;

    private String content;

    private String thumb;

    private String shareurl;

    private String description;

    private String type;

    private ArrayList<GalleryItemBean> gallery;

    private int favorited;

    private int liked;

    private int likenum;

    private int commentnum;

    private int issubscribecate; //栏目是否可订阅

    private int subscribed; // 是否已经订阅

    private String catthumb;

    private String catname;

    private String url; // 收藏时使用

    private String adtitle;

    private String adthumb;

    private String adshareicon;

    private String adtype;

    private String adurl;

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCopyfrom() {
        return copyfrom;
    }

    public void setCopyfrom(String copyfrom) {
        this.copyfrom = copyfrom;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getShareurl() {
        return shareurl;
    }

    public void setShareurl(String shareurl) {
        this.shareurl = shareurl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<GalleryItemBean> getGallery() {
        return gallery;
    }

    public void setGallery(ArrayList<GalleryItemBean> gallery) {
        this.gallery = gallery;
    }

    public int getCommentnum() {
        return commentnum;
    }

    public void setCommentnum(int commentnum) {
        this.commentnum = commentnum;
    }

    public int getFavorited() {
        return favorited;
    }

    public void setFavorited(int favorited) {
        this.favorited = favorited;
    }

    public int getLiked() {
        return liked;
    }

    public void setLiked(int liked) {
        this.liked = liked;
    }

    public int getLikenum() {
        return likenum;
    }

    public void setLikenum(int likenum) {
        this.likenum = likenum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getIssubscribecate() {
        return issubscribecate;
    }

    public void setIssubscribecate(int issubscribecate) {
        this.issubscribecate = issubscribecate;
    }

    public int getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(int subscribed) {
        this.subscribed = subscribed;
    }

    public String getCatthumb() {
        return catthumb;
    }

    public void setCatthumb(String catthumb) {
        this.catthumb = catthumb;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getAdtitle() {
        return adtitle;
    }

    public void setAdtitle(String adtitle) {
        this.adtitle = adtitle;
    }

    public String getAdthumb() {
        return adthumb;
    }

    public void setAdthumb(String adthumb) {
        this.adthumb = adthumb;
    }

    public String getAdshareicon() {
        return adshareicon;
    }

    public void setAdshareicon(String adshareicon) {
        this.adshareicon = adshareicon;
    }

    public String getAdtype() {
        return adtype;
    }

    public void setAdtype(String adtype) {
        this.adtype = adtype;
    }

    public String getAdurl() {
        return adurl;
    }

    public void setAdurl(String adurl) {
        this.adurl = adurl;
    }
}
