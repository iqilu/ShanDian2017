package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/8/17.
 */
public class ShakeResultBean {

    private String title;

    private int prize; //未中奖此项为0 中奖此项为1

    private int begin;

    private ShakeBean info;

    private PrizeinfoBean prizeinfo;

    private String secret;

    private String starttime;

    private String endtime;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public int getBegin() {
        return begin;
    }

    public void setBegin(int begin) {
        this.begin = begin;
    }

    public ShakeBean getInfo() {
        return info;
    }

    public void setInfo(ShakeBean info) {
        this.info = info;
    }

    public PrizeinfoBean getPrizeinfo() {
        return prizeinfo;
    }

    public void setPrizeinfo(PrizeinfoBean prizeinfo) {
        this.prizeinfo = prizeinfo;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}
