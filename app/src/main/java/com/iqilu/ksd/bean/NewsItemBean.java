package com.iqilu.ksd.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 新闻列表Item
 * <p/>
 * Created by coofee on 2015/11/5.
 */
public class NewsItemBean implements Serializable {

    private int id;

    private String title;

    private String shorttitle;

    private int catid;

    private String catname;

    private String thumb;

    private String shareicon;

    private String video;

    private List<String> gallery; //图集

    private int isbigimage;

    private String type;

    private String realtype;

    private String url;

    private String date;

    private int likenum; // 点赞

    private String bigimage;

    private ArrayList<VideoBean> videos;

    private String imgsrc;

    private ArrayList<BannerBean> bannerList;

    private String novisitnum;

    private int isgroup;

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<String> getGallery() {
        return gallery;
    }

    public void setGallery(List<String> gallery) {
        this.gallery = gallery;
    }

    public int getIsbigimage() {
        return isbigimage;
    }

    public void setIsbigimage(int isbigimage) {
        this.isbigimage = isbigimage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getShorttitle() {
        return shorttitle;
    }

    public void setShorttitle(String shorttitle) {
        this.shorttitle = shorttitle;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getShareicon() {
        return shareicon;
    }

    public void setShareicon(String shareicon) {
        this.shareicon = shareicon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRealtype() {
        return realtype;
    }

    public void setRealtype(String realtype) {
        this.realtype = realtype;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ArrayList<VideoBean> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<VideoBean> videos) {
        this.videos = videos;
    }

    public int getLikenum() {
        return likenum;
    }

    public void setLikenum(int likenum) {
        this.likenum = likenum;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getImgsrc() {
        return imgsrc;
    }

    public void setImgsrc(String imgsrc) {
        this.imgsrc = imgsrc;
    }

    public String getBigimage() {
        return bigimage;
    }

    public void setBigimage(String bigimage) {
        this.bigimage = bigimage;
    }

    public ArrayList<BannerBean> getBannerList() {
        return bannerList;
    }

    public void setBannerList(ArrayList<BannerBean> bannerList) {
        this.bannerList = bannerList;
    }

    public String getNovisitnum() {
        return novisitnum;
    }

    public void setNovisitnum(String novisitnum) {
        this.novisitnum = novisitnum;
    }

    public int getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(int isgroup) {
        this.isgroup = isgroup;
    }
}
