package com.iqilu.ksd.bean;

/**
 * 用户信息字段
 * <p>
 * Created by coofee on 2015/11/13.
 */
public class UserBean {

    private int id;

    private String phone;

    private String nickname;

    private String avatar;

    private int sex;

    private String birthday;

    private String lastlogintime;

    private String loginhash;

    private String hash;

    private int role;

    private int favoritenum;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastlogintime() {
        return lastlogintime;
    }

    public void setLastlogintime(String lastlogintime) {
        this.lastlogintime = lastlogintime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getLoginhash() {
        return loginhash;
    }

    public void setLoginhash(String loginhash) {
        this.loginhash = loginhash;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getFavoritenum() {
        return favoritenum;
    }

    public void setFavoritenum(int favoritenum) {
        this.favoritenum = favoritenum;
    }
}
