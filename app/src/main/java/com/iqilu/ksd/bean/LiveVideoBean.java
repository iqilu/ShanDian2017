package com.iqilu.ksd.bean;

import java.io.Serializable;

/**
 * Created by Coofee on 2016/8/9.
 */
public class LiveVideoBean implements Serializable{

    private int id;

    private String title;

    private String poster;

    private String video;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
}
