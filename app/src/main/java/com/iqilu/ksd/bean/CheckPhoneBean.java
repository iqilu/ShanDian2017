package com.iqilu.ksd.bean;

/**
 * 检查手机接口的返回值
 * <p/>
 * Created by coofee on 2015/11/18.
 */
public class CheckPhoneBean {

    private String verifycode;

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getVerifycode() {
        return verifycode;
    }

    public void setVerifycode(String verifycode) {
        this.verifycode = verifycode;
    }
}
