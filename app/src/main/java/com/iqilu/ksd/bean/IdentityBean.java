package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/4/27.
 */
public class IdentityBean {

    private int verify;

    private String msg;

    public int getVerify() {
        return verify;
    }

    public void setVerify(int verify) {
        this.verify = verify;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
