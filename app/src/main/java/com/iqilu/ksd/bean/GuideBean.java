package com.iqilu.ksd.bean;

import java.util.ArrayList;

/**
 * 节目单
 * Created by Coofee on 2016/11/27.
 */

public class GuideBean {

    private String catname;

    private String date;

    private ArrayList<GuideChildBean> cates;

    private String name;

    private String start;

    private String end;

    private int playing;

    private int played;

    private int catid;

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public int getPlaying() {
        return playing;
    }

    public void setPlaying(int playing) {
        this.playing = playing;
    }

    public int getPlayed() {
        return played;
    }

    public void setPlayed(int played) {
        this.played = played;
    }

    public ArrayList<GuideChildBean> getCates() {
        return cates;
    }

    public void setCates(ArrayList<GuideChildBean> cates) {
        this.cates = cates;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }
}
