package com.iqilu.ksd.bean;

/**
 * 电视and广播
 * Created by Coofee on 2016/11/25.
 */

public class TVBean {

    private int id;

    private String name;

    private String playing;

    private String img;

    private int imgResorceId;

    private int titleResorceId;

    private String url;

    private int coverImgId;

    private int smallCoverImgId;

    private String path;

    public TVBean(int id, int titleResorceId, int imgResorceId, String url, String path) {
        this.id = id;
        this.imgResorceId = imgResorceId;
        this.titleResorceId = titleResorceId;
        this.url = url;
        this.path = path;
    }

    public TVBean(int id, int titleResorceId, int imgResorceId, String url, int coverImgId, int smallCoverImgId) {
        this.coverImgId = coverImgId;
        this.id = id;
        this.imgResorceId = imgResorceId;
        this.titleResorceId = titleResorceId;
        this.url = url;
        this.smallCoverImgId = smallCoverImgId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlaying() {
        return playing;
    }

    public void setPlaying(String playing) {
        this.playing = playing;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public int getImgResorceId() {
        return imgResorceId;
    }

    public void setImgResorceId(int imgResorceId) {
        this.imgResorceId = imgResorceId;
    }

    public int getTitleResorceId() {
        return titleResorceId;
    }

    public void setTitleResorceId(int titleResorceId) {
        this.titleResorceId = titleResorceId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getCoverImgId() {
        return coverImgId;
    }

    public void setCoverImgId(int coverImgId) {
        this.coverImgId = coverImgId;
    }

    public int getSmallCoverImgId() {
        return smallCoverImgId;
    }

    public void setSmallCoverImgId(int smallCoverImgId) {
        this.smallCoverImgId = smallCoverImgId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
