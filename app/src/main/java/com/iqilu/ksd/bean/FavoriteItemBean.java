package com.iqilu.ksd.bean;

/**
 * 我的收藏列表页
 * Created by coofee on 2015/11/30.
 */
public class FavoriteItemBean {

    private int id;

    private int memberid;

    private String title;

    private String type;

    private int catid;

    private int articleid;

    private String url;

    private long addline;

    private String date;

    private String thumb;

    public long getAddline() {
        return addline;
    }

    public void setAddline(long addline) {
        this.addline = addline;
    }

    public int getArticleid() {
        return articleid;
    }

    public void setArticleid(int articleid) {
        this.articleid = articleid;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMemberid() {
        return memberid;
    }

    public void setMemberid(int memberid) {
        this.memberid = memberid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
