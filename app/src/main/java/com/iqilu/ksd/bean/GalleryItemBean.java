package com.iqilu.ksd.bean;

import java.io.Serializable;

/**
 * 图集信息字段
 * <p/>
 * Created by coofee on 2015/11/5.
 */
public class GalleryItemBean implements Serializable {


    private String img;

    private String text;

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
