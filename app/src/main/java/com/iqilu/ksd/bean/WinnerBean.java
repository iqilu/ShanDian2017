package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2017/7/28.
 */

public class WinnerBean {

    private String name;

    private String phone;

    private String prize;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }
}
