package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/8/22.
 */
public class PrizeinfoBean {

    private int id;

    private int uid;

    private int rockid;

    private String level;

    private String title;

    private String thumb;

    private String noprizetip;

    private String secret;

    private int num;

    private int remain;

    private int isneedinfo;

    private int isdel;

    private String create_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getRockid() {
        return rockid;
    }

    public void setRockid(int rockid) {
        this.rockid = rockid;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public int getIsneedinfo() {
        return isneedinfo;
    }

    public void setIsneedinfo(int isneedinfo) {
        this.isneedinfo = isneedinfo;
    }

    public int getIsdel() {
        return isdel;
    }

    public void setIsdel(int isdel) {
        this.isdel = isdel;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getNoprizetip() {
        return noprizetip;
    }

    public void setNoprizetip(String noprizetip) {
        this.noprizetip = noprizetip;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }
}
