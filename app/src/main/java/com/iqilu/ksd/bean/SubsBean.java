package com.iqilu.ksd.bean;

import java.util.ArrayList;

/**
 * 订阅
 * Created by coofee on 2015/11/17.
 */
public class SubsBean {

    private int catid;

    private String catname;

    private String type;

    private int subscribed;

    private Boolean clicked; // 是否被点击

    private ArrayList<SubsItemBean> cates;

    public ArrayList<SubsItemBean> getCates() {
        return cates;
    }

    public void setCates(ArrayList<SubsItemBean> cates) {
        this.cates = cates;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(int subscribed) {
        this.subscribed = subscribed;
    }

    public Boolean getClicked() {
        return clicked;
    }

    public void setClicked(Boolean clicked) {
        this.clicked = clicked;
    }
}
