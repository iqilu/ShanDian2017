package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2017/4/21.
 */

public class VoteSignBean {

    private String title;

    private String thumb;

    private String url;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
