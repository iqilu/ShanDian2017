package com.iqilu.ksd.bean;

import java.util.ArrayList;

/**
 * 节目单
 * Created by Coofee on 2016/11/27.
 */

public class GuideChildBean {

    private int catid;

    private String date;

    private String catname;

    private String thumb;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public int getCatid() {
        return catid;
    }

    public void setCatid(int catid) {
        this.catid = catid;
    }
}
