package com.iqilu.ksd.bean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2017/4/19.
 */

public class SubscribeBean {

    private String subscribehash;

    private ArrayList<NewsBean> cates; //订阅栏目列表

    private ArrayList<NewsItemBean> infos;

    private ArrayList<NewsItemBean> jxinfos; //精选推荐

    private ArrayList<SubsItemBean> tjinfos; //推荐订阅号

    public ArrayList<NewsBean> getCates() {
        return cates;
    }

    public void setCates(ArrayList<NewsBean> cates) {
        this.cates = cates;
    }

    public ArrayList<NewsItemBean> getInfos() {
        return infos;
    }

    public void setInfos(ArrayList<NewsItemBean> infos) {
        this.infos = infos;
    }

    public ArrayList<NewsItemBean> getJxinfos() {
        return jxinfos;
    }

    public void setJxinfos(ArrayList<NewsItemBean> jxinfos) {
        this.jxinfos = jxinfos;
    }

    public ArrayList<SubsItemBean> getTjinfos() {
        return tjinfos;
    }

    public void setTjinfos(ArrayList<SubsItemBean> tjinfos) {
        this.tjinfos = tjinfos;
    }

    public String getSubscribehash() {
        return subscribehash;
    }

    public void setSubscribehash(String subscribehash) {
        this.subscribehash = subscribehash;
    }
}
