package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/12/29.
 */

public class BoxBean {

    private String baoming;

    private String toupiao;

    public String getBaoming() {
        return baoming;
    }

    public void setBaoming(String baoming) {
        this.baoming = baoming;
    }

    public String getToupiao() {
        return toupiao;
    }

    public void setToupiao(String toupiao) {
        this.toupiao = toupiao;
    }
}
