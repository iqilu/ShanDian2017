package com.iqilu.ksd.bean;

import java.util.ArrayList;

/**
 * 直播
 * Created by Coofee on 2016/4/8.
 */
public class LiveBean {

    private int id;

    private int uid;

    private int isgroup;

    private String title;

    private String grouptitle;

    private String type;

    private String litpic;

    private String tplstyle;

    private String tpl;

    private int isdel;

    private int islock;

    private int live_count;

    private int comment_count;

    private int click;

    private int livenum;

    private int livestatus;

    private String livestream;

    private String create_at;

    private String update_at;

    private String avatar;

    private String nickname;

    private String shareurl;

    private String description;

    private String shareicon;

    private int favorited; //是否收藏

    private String date;

    private String liverolename;

    private ArrayList<LiveVideoBean> videos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLitpic() {
        return litpic;
    }

    public void setLitpic(String litpic) {
        this.litpic = litpic;
    }

    public String getTplstyle() {
        return tplstyle;
    }

    public void setTplstyle(String tplstyle) {
        this.tplstyle = tplstyle;
    }

    public String getTpl() {
        return tpl;
    }

    public void setTpl(String tpl) {
        this.tpl = tpl;
    }

    public int getIsdel() {
        return isdel;
    }

    public void setIsdel(int isdel) {
        this.isdel = isdel;
    }

    public int getIslock() {
        return islock;
    }

    public void setIslock(int islock) {
        this.islock = islock;
    }

    public int getLive_count() {
        return live_count;
    }

    public void setLive_count(int live_count) {
        this.live_count = live_count;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public int getClick() {
        return click;
    }

    public void setClick(int click) {
        this.click = click;
    }

    public int getLivestatus() {
        return livestatus;
    }

    public void setLivestatus(int livestatus) {
        this.livestatus = livestatus;
    }

    public String getLivestream() {
        return livestream;
    }

    public void setLivestream(String livestream) {
        this.livestream = livestream;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getShareurl() {
        return shareurl;
    }

    public void setShareurl(String shareurl) {
        this.shareurl = shareurl;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShareicon() {
        return shareicon;
    }

    public void setShareicon(String shareicon) {
        this.shareicon = shareicon;
    }

    public int getFavorited() {
        return favorited;
    }

    public void setFavorited(int favorited) {
        this.favorited = favorited;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLiverolename() {
        return liverolename;
    }

    public void setLiverolename(String liverolename) {
        this.liverolename = liverolename;
    }

    public ArrayList<LiveVideoBean> getVideos() {
        return videos;
    }

    public void setVideos(ArrayList<LiveVideoBean> videos) {
        this.videos = videos;
    }

    public int getIsgroup() {
        return isgroup;
    }

    public void setIsgroup(int isgroup) {
        this.isgroup = isgroup;
    }

    public String getGrouptitle() {
        return grouptitle;
    }

    public void setGrouptitle(String grouptitle) {
        this.grouptitle = grouptitle;
    }

    public int getLivenum() {
        return livenum;
    }

    public void setLivenum(int livenum) {
        this.livenum = livenum;
    }
}
