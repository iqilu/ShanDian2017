package com.iqilu.ksd.bean;

/**
 * 城市item
 * <p/>
 * Created by coofee on 2015/11/11.
 */
public class CityItemBean {

    private int cityId;

    private String cityName;

    public CityItemBean(int cityId, String cityName) {
        this.cityId = cityId;
        this.cityName = cityName;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
