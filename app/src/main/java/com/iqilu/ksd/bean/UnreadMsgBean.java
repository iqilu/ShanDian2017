package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/5/18.
 */
public class UnreadMsgBean {

    private int has;

    public int getHas() {
        return has;
    }

    public void setHas(int has) {
        this.has = has;
    }
}
