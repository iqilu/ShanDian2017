package com.iqilu.ksd.bean;

/**
 * Created by Coofee on 2016/5/3.
 */
public class DefaultImageBean {

    private String title;

    private String thumb;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
