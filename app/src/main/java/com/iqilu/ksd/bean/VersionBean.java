package com.iqilu.ksd.bean;

/**
 * 检查新版本
 * Created by coofee on 2015/12/1.
 */
public class VersionBean {

    private String androidversion;

    private String android;

    public String getAndroid() {
        return android;
    }

    public void setAndroid(String android) {
        this.android = android;
    }

    public String getAndroidversion() {
        return androidversion;
    }

    public void setAndroidversion(String androidversion) {
        this.androidversion = androidversion;
    }
}
