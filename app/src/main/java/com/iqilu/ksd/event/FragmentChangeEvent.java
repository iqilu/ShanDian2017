package com.iqilu.ksd.event;

/**
 * Created by Coofee on 2017/4/11.
 */

public class FragmentChangeEvent {

    public int id;

    public FragmentChangeEvent(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
