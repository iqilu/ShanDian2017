package com.iqilu.ksd.event;

/**
 * Created by Coofee on 2016/5/18.
 */
public class UnreadMsgEvent {

    int count;

    public UnreadMsgEvent(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
