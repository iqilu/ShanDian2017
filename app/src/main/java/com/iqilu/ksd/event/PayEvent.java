package com.iqilu.ksd.event;

/**
 * Created by Coofee on 2016/7/26.
 */
public class PayEvent {

    boolean paySuccess;

    public PayEvent(boolean paySuccess) {
        this.paySuccess = paySuccess;
    }

    public boolean isPaySuccess() {
        return paySuccess;
    }

    public void setPaySuccess(boolean paySuccess) {
        this.paySuccess = paySuccess;
    }
}
