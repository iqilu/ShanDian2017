package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.LiveAdapter;
import com.iqilu.ksd.adapter.VoteSignAdapter;
import com.iqilu.ksd.bean.LiveBean;
import com.iqilu.ksd.bean.VoteSignBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.VoteSign;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * 投票 and 报名列表页
 * Created by Coofee on 2017/3/22.
 */

public class VoteSignActivity extends BaseActivity {

    private int page;
    private ArrayList<VoteSignBean> list;
    private VoteSignAdapter adapter;

    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private ImageView btReload;
    private RelativeLayout layLoading;
    private RelativeLayout layEmpty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_votesign);

        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        adapter = new VoteSignAdapter(this);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        rvList.setAdapter(adapter);
        adapter.setOnItemClickListener(new VoteSignAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                Intent intent = new Intent(VoteSignActivity.this,ADActivity.class);
                intent.putExtra("showShareBtn",false);
                intent.putExtra("adUrl",list.get(position).getUrl());
                startActivity(intent);
            }
        });
        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });

        btReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = 1;
                getList();
                layLoading.setVisibility(View.VISIBLE);
                xRefreshView.setVisibility(View.VISIBLE);
            }
        });
        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        btReload = getView(R.id.bt_reload);
        layLoading = getView(R.id.lay_loading);
        layEmpty = getView(R.id.lay_empty);

        if(getIntent().getStringExtra("type").equals(VoteSign.VOTE)) {
            tvTitle.setText(getString(R.string.vote_title));
        }else {
            tvTitle.setText(getString(R.string.sign_title));
        }
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    private void getList() {
        String url;
        if(getIntent().getStringExtra("type").equals(VoteSign.VOTE)){
            url = Api.URL_BMTP_TOUPIAO;
        }else {
            url = Api.URL_BMTP_BAOMING;
        }
        url = url + "?page="+page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
                if (page == 1) {
                    setEmpty();
                }
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<VoteSignBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<VoteSignBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        setEmpty();
                        Toast.makeText(VoteSignActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list = result;
                    }
                } else {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(VoteSignActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                    }
                }
                adapter.setData(list);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void afterRequest() {
        layLoading.setVisibility(View.GONE);
        layEmpty.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }

    private void setEmpty() {
        adapter.setData(null);
        adapter.notifyDataSetChanged();
        layEmpty.setVisibility(View.VISIBLE);
        xRefreshView.setVisibility(View.GONE);
    }
}
