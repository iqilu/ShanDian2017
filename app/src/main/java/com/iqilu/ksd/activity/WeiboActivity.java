package com.iqilu.ksd.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.constant.Config;
import com.jaeger.library.StatusBarUtil;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;

/**
 * 新浪微博分享界面
 * Created by coofee on 2015/12/17.
 */
public class WeiboActivity extends BaseActivity {

    private static String TAG = "WeiboActivity";

    private String content;
    private String shareUrl;
    private String thumb;

    private EditText etTitle;
    private TextView tvLeft;
    private SimpleDraweeView imgThumb;
    private ImageView imgDel;
    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView btRight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weibo);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);

        Intent intent = getIntent();
        content = intent.getStringExtra("title");
        shareUrl = intent.getStringExtra("shareUrl");
        content = content + getString(R.string.share_detail) + shareUrl;
        thumb = intent.getStringExtra("thumb");

        init();
    }

    private void init() {
        etTitle = getView(R.id.et_title);
        tvLeft = getView(R.id.tv_left);
        imgThumb = getView(R.id.img_thumb);
        imgDel = getView(R.id.img_del);
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        btRight = getView(R.id.bt_right);
        btRight.setImageResource(R.drawable.bt_save);
        btRight.setVisibility(View.VISIBLE);
        tvTitle.setText(getString(R.string.weibo_title));
        etTitle.setText(content);
        etTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkLeftNum();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        imgDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                thumb = "";
                imgThumb.setVisibility(View.GONE);
            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                share();
            }
        });

        if (!TextUtils.isEmpty(thumb)) {
            imgThumb.setImageURI(Uri.parse(thumb));
        }
        int leftLength = 2000 - content.length();
        tvLeft.setText("" + leftLength);
        if (leftLength >= 0) {
            tvLeft.setTextColor(getResources().getColor(R.color.weibo_blue));
        } else {
            tvLeft.setTextColor(getResources().getColor(R.color.weibo_red));
        }
    }

    private void checkLeftNum() {
        int leftLength = 2000 - etTitle.length();
        tvLeft.setText("" + leftLength);
        if (leftLength >= 0) {
            tvLeft.setTextColor(getResources().getColor(R.color.weibo_blue));
        } else {
            tvLeft.setTextColor(getResources().getColor(R.color.weibo_red));
        }
    }


    private void share() {
        ShareSDK.initSDK(this);
        Platform.ShareParams sp = new Platform.ShareParams();
        Platform platform = ShareSDK.getPlatform(SinaWeibo.NAME);
        sp.setText("" + etTitle.getText());
        sp.setImageUrl(thumb);
        sp.setSite(getString(R.string.app_name));
        platform.SSOSetting(false);
        platform.showUser(null);
        if (!platform.isValid()) {
            platform.authorize();
        }
        platform.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                handler.sendEmptyMessage(1);
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                handler.sendEmptyMessage(0);
            }

            @Override
            public void onCancel(Platform platform, int i) {

            }
        });
        // 执行分享
        platform.share(sp);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 1) {
                Toast.makeText(WeiboActivity.this, R.string.share_success, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(WeiboActivity.this, R.string.share_error, Toast.LENGTH_SHORT).show();
            }
            finish();
        }
    };
}
