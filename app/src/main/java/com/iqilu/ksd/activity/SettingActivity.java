package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.iqilu.ksd.R;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.BaseUtils;
import com.iqilu.ksd.utils.CacheUtils;
import com.iqilu.ksd.utils.ConvertUtils;
import com.iqilu.ksd.utils.FileUtils;
import com.iqilu.ksd.utils.UpdateUtils;
import com.jaeger.library.StatusBarUtil;

import java.io.File;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by Coofee on 2016/10/18.
 */

public class SettingActivity extends BaseActivity implements View.OnClickListener {

    private static String TAG = "SettingActivity";

    private String cacheDir;

    private ImageView btLeft;
    private TextView tvTitle;
    private RelativeLayout layCache;
    private ToggleButton btPush;
    private RelativeLayout layCheck;
    private RelativeLayout layAbout;
    private TextView tvVersion;
    private TextView tvCache;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        init();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        layCache = getView(R.id.lay_cache);
        btPush = getView(R.id.bt_push);
        layCheck = getView(R.id.lay_check);
        layAbout = getView(R.id.lay_about);
        tvVersion = getView(R.id.tv_version);
        tvCache = getView(R.id.tv_cache);
        btLeft.setOnClickListener(this);
        tvTitle.setText(getString(R.string.setting_title));
        layCheck.setOnClickListener(this);
        layCache.setOnClickListener(this);
        layAbout.setOnClickListener(this);
        btPush.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    JPushInterface.resumePush(SettingActivity.this);
                } else {
                    JPushInterface.stopPush(SettingActivity.this);
                }
            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void init() {
        // 推送
        if (JPushInterface.isPushStopped(this)) {
            btPush.setChecked(false);
        } else {
            btPush.setChecked(true);
        }

        // 缓存
        displayCache();

        // 版本
        tvVersion.setText("" + BaseUtils.getVersion(this));
    }

    // 显示缓存大小
    public void displayCache() {
        long cacheSize = 0;
        String txtCacheSize = "";
        cacheDir = CacheUtils.getCacheDir(this);
        File file = new File(cacheDir);
        try {
            cacheSize = FileUtils.getFolderSize(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        txtCacheSize = ConvertUtils.byte2FitSize(cacheSize);
        tvCache.setText(txtCacheSize);
    }

    public void clearCache() {
        final MaterialDialog dialog = new MaterialDialog(this);
        dialog.content(getString(R.string.setting_clear_tip))
                .btnText(getString(R.string.cancel), getString(R.string.ensure))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        FileUtils.deleteFolderFile(cacheDir, false);
                        Toast.makeText(SettingActivity.this, R.string.setting_clear_success, Toast.LENGTH_SHORT).show();
                        displayCache();
                        dialog.dismiss();
                    }
                }
        );
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_left:
                break;
            case R.id.lay_cache:
                clearCache();
                break;
            case R.id.lay_check:
                new UpdateUtils(this, true).checkUpdate(BaseUtils.getVersion(this));
                break;
            case R.id.lay_about:
                startActivity(new Intent(this, AboutActivity.class));
                break;
            default:
                break;
        }
    }
}
