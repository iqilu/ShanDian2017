package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.LiveAdapter;
import com.iqilu.ksd.adapter.ShakeListAdapter;
import com.iqilu.ksd.bean.LiveBean;
import com.iqilu.ksd.bean.ShakeListBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.JSONUtils;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2017/7/21.
 */

public class ShakeListActivity extends BaseActivity implements View.OnClickListener{

    private ImageView btLeft;
    private TextView tvTitle;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private ImageView imgEmpty;
    private RelativeLayout layLoading;

    private int page = 1;
    private ArrayList<ShakeListBean> list;
    private ShakeListAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
        adapter = new ShakeListAdapter(this);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(new ShakeListAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                ShakeListBean data = list.get(position);
                Intent intent = new Intent(ShakeListActivity.this,ShakeActivity.class);
                intent.putExtra("id",data.getId());
                startActivity(intent);
            }
        });
        rvList.setAdapter(adapter);
        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.enableReleaseToLoadMore(true);
        xRefreshView.enableRecyclerViewPullUp(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });

        getList();
    }

    private void initView(){
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        imgEmpty = getView(R.id.img_empty);
        layLoading = getView(R.id.lay_loading);

        btLeft.setOnClickListener(this);
        layLoading.setVisibility(View.VISIBLE);
    }

    private void getList(){
        String url = Api.URL_ROCK_LIST+"?page="+page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {

            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<ShakeListBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<ShakeListBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(ShakeListActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        imgEmpty.setVisibility(View.VISIBLE);
                    } else {
                        list = result;
                    }
                } else {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(ShakeListActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                    }
                }
                adapter.setData(list);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void afterRequest() {
        layLoading.setVisibility(View.GONE);
        imgEmpty.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bt_left:
                finish();
                break;
        }

    }
}
