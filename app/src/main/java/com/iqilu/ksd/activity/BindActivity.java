package com.iqilu.ksd.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.iqilu.ksd.MyApplication;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.UserBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.SmssdkConfig;
import com.iqilu.ksd.constant.UserInfo;
import com.iqilu.ksd.event.ChangeUserInfoEvent;
import com.iqilu.ksd.utils.BaseUtils;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.utils.SPUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import cn.smssdk.EventHandler;
import cn.smssdk.SMSSDK;
import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2017/7/20.
 */

public class BindActivity extends BaseActivity implements View.OnClickListener{

    private static String APPKEY = SmssdkConfig.APPKEY;
    private static String APPSECRET = SmssdkConfig.APPSECRET;

    private LigDialog ligDialog;
    public String phone;
    public String password;
    public String nickname;
    public String code;
    public static int sendSmsStart = 888;
    public static int sendSmsEnd = 889;

    private ImageView btLeft;
    private TextView tvTitle;
    private EditText etTelephone;
    private EditText etVerify;
    private EditText etPassword;
    private Button btGetverify;
    private Button btSubmit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bind);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();

        SMSSDK.initSDK(this, APPKEY, APPSECRET);
        EventHandler eh = new EventHandler() {
            @Override
            public void afterEvent(int event, int result, Object data) {

                Message msg = new Message();
                msg.arg1 = event;
                msg.arg2 = result;
                msg.obj = data;
                handler.sendMessage(msg);
            }

        };
        SMSSDK.registerEventHandler(eh);
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        etTelephone = getView(R.id.et_telephone);
        etVerify = getView(R.id.et_verify);
        etPassword = getView(R.id.et_password);
        btGetverify = getView(R.id.bt_getverify);
        btSubmit = getView(R.id.bt_submit);

        tvTitle.setText(R.string.login_register);
        btLeft.setOnClickListener(this);
        btGetverify.setOnClickListener(this);
        btSubmit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_left:
                finish();
                break;
            case R.id.bt_getverify:
                getVerify();
                break;
            case R.id.bt_submit:
                toSubmit();
                break;
            default:
                break;
        }
    }

    private void toSubmit() {
        String url = Api.URL_USER_BINDPHONE;
        phone = etTelephone.getText().toString();
        code = etVerify.getText().toString();
        password = etPassword.getText().toString();
        if (!matchPhone(phone)) {
            Toast.makeText(this, R.string.login_telephone_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(code)) {
            Toast.makeText(this, R.string.register_input_verify, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.login_pass_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!BaseUtils.isPassword(password)) {
            Toast.makeText(this, R.string.login_pass_error, Toast.LENGTH_SHORT).show();
            return;
        }
        OkHttpUtils.post()
                .url(url)
                .addParams("phone",phone)
                .addParams("code",code)
                .addParams("password",password)
                .addParams("platform","android")
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        ligDialog = new LigDialog(BindActivity.this, getString(R.string.login_doing));
                        ligDialog.show();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ligDialog.dismiss();
                        Toast.makeText(BindActivity.this,R.string.net_error,Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ligDialog.dismiss();
                        UserBean user = null;
                        JSONObject jsonObject = null;
                        int status = 0;
                        try {
                            jsonObject = new JSONObject(response);
                            status = jsonObject.getInt("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(status == 1){
                            user = JSONUtils.requestDetail(response, "data", UserBean.class);
                            ((MyApplication) getApplication()).setUser(user);
                            SPUtils.setPref(BindActivity.this, UserInfo.LOGINHASH, user.getLoginhash());
                            afterLogin();
                            Toast.makeText(BindActivity.this, R.string.login_success, Toast.LENGTH_SHORT).show();
                            finish();
                        }else if(status == 2){ //手机号码已经存在
                            AlertDialog.Builder builder = new AlertDialog.Builder(BindActivity.this);
                            builder.setMessage(R.string.bind_existence);
                            builder.setNegativeButton(R.string.cancel, null);
                            builder.setPositiveButton(R.string.ensure, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            builder.show();
                        }else {
                            Toast.makeText(BindActivity.this,R.string.net_error,Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    void getVerify() {
        phone = etTelephone.getText().toString();
        if (!matchPhone(phone)) {
            Toast.makeText(this, R.string.login_telephone_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        SMSSDK.getVerificationCode("86", phone);
        new Thread(new Runnable() {
            public int num = 60;

            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    num--;
                    Message msg = new Message();
                    msg.arg1 = num;
                    msg.arg2 = sendSmsStart;
                    if (num <= 0) {
                        msg.arg2 = sendSmsEnd;
                        handler.sendMessage(msg);
                        num = 60;
                        break;
                    } else {
                        handler.sendMessage(msg);
                    }
                }
            }
        }).start();

    }

    // 验证手机号
    public Boolean matchPhone(String phoneNum) {
        if (phoneNum.length() < 11) {
            return false;
        }
        Pattern pattern = Pattern.compile("[0-9]{1,}");
        Matcher matcher = pattern.matcher((CharSequence) phoneNum);
        return matcher.matches();
    }

    Handler handler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            int event = msg.arg1;
            int result = msg.arg2;
            Object data = msg.obj;
            if (result == SMSSDK.RESULT_COMPLETE) {
                //短信注册成功后，返回MainActivity,然后提示新好友
                if (event == SMSSDK.EVENT_SUBMIT_VERIFICATION_CODE) {//提交验证码成功
                } else if (event == SMSSDK.EVENT_GET_VERIFICATION_CODE) {
                    Toast.makeText(getApplicationContext(), R.string.register_verifycode_send, Toast.LENGTH_SHORT).show();
                } else if (event == SMSSDK.EVENT_GET_SUPPORTED_COUNTRIES) {//返回支持发送验证码的国家列表

                }
            } else if (result == sendSmsStart) {
                btGetverify.setEnabled(false);
                btGetverify.setText((getResources().getString(R.string.register_send_sms_again) + event));
            } else if (result == sendSmsEnd) {
                btGetverify.setEnabled(true);
                btGetverify.setText(getResources().getString(R.string.register_send_verify));
            }
        }

    };

    private void afterLogin() {
        EventBus.getDefault().post(new ChangeUserInfoEvent());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SMSSDK.unregisterAllEventHandler();
    }
}
