package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.MyFavoriteAdapter;
import com.iqilu.ksd.bean.FavoriteItemBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.NewsType;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2016/10/18.
 */

public class MyFavoriteActivity extends BaseActivity {

    private static final String TAG = "MyFavoriteActivity";

    private int page;
    private ArrayList<FavoriteItemBean> list;
    private MyFavoriteAdapter adapter;

    private LigDialog ligDialog;
    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private RelativeLayout layLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        adapter = new MyFavoriteAdapter(this);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(onItemClickListener);
        rvList.setAdapter(adapter);
        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });
        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        imgEmpty.setImageResource(R.drawable.bg_fav_empty);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(getString(R.string.favorite_title));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    private void getList() {
        String url = Api.URL_FAVORITE + "?page=" + page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
                if(page == 1){
                    xRefreshView.setVisibility(View.GONE);
                    imgEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<FavoriteItemBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<FavoriteItemBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        imgEmpty.setVisibility(View.VISIBLE);
                        xRefreshView.setVisibility(View.GONE);
                        Toast.makeText(MyFavoriteActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list = result;
                        adapter.setData(list);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    xRefreshView.stopLoadMore();
                    if (result == null || result.size() == 0) {
                        Toast.makeText(MyFavoriteActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                        adapter.setData(list);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    private void afterRequest() {
        layLoading.setVisibility(View.GONE);
        imgEmpty.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }

    public void del(final int position) {
        final MaterialDialog dialog = new MaterialDialog(this);
        dialog.content(this.getString(R.string.live_del))
                .btnText(this.getString(R.string.cancel), this.getString(R.string.ensure))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        delFavorite(position);
                        dialog.dismiss();
                    }
                }
        );
    }

    private void delFavorite(final int position) {
        int id = list.get(position).getArticleid();
        final String type = list.get(position).getType();
        String url = Api.URL_FAVORITE + "?articleid=" + id + "&type=" + type;
        OkHttpUtils.delete().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                ligDialog = new LigDialog(MyFavoriteActivity.this, getString(R.string.waiting));
                ligDialog.show();
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                ligDialog.dismiss();
            }

            @Override
            public void onResponse(String response, int id) {
                ligDialog.dismiss();
                int status = 0;
                status = JSONUtils.filterInt(response, "status", 0);
                if (status == 1) {
                    Toast.makeText(MyFavoriteActivity.this, R.string.favorite_del_success, Toast.LENGTH_SHORT).show();
                    list.remove(position);
                    adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(MyFavoriteActivity.this, R.string.favorite_del_error, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private MyFavoriteAdapter.OnItemClickListener onItemClickListener = new MyFavoriteAdapter.OnItemClickListener() {
        @Override
        public void OnItemClickListener(View view, int position) {
            Intent intent = null;
            FavoriteItemBean item = list.get(position);
            int id = item.getArticleid();
            int catId = item.getCatid();
            if (item.getType().equals(NewsType.ARTICLE)) {
                intent = new Intent(MyFavoriteActivity.this, NewsDetailActivity.class);
            } else if (item.getType().equals(NewsType.GALLERY)) {
                intent = new Intent(MyFavoriteActivity.this, GalleryActivity.class);
                intent.putExtra("position", 0);
            } else if (item.getType().equals(NewsType.LIVE)) {
                intent = new Intent(MyFavoriteActivity.this, LiveActivity.class);
            } else if (item.getType().equals(NewsType.CLUE)) {
                intent = new Intent(MyFavoriteActivity.this, PaikeActivity.class);
            }
            if (intent != null) {
                intent.putExtra("id", id);
                intent.putExtra("catId", catId);
                startActivity(intent);
            }
        }
    };
}
