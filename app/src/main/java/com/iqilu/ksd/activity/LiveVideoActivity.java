package com.iqilu.ksd.activity;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;

import com.android.tedcoder.wkvideoplayer.constant.VideoType;
import com.android.tedcoder.wkvideoplayer.model.Video;
import com.android.tedcoder.wkvideoplayer.model.VideoUrl;
import com.android.tedcoder.wkvideoplayer.util.DensityUtil;
import com.android.tedcoder.wkvideoplayer.view.MediaController;
import com.android.tedcoder.wkvideoplayer.view.SuperVideoPlayer;
import com.iqilu.ksd.R;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/23.
 */

public class LiveVideoActivity extends BaseActivity {

    SuperVideoPlayer superVideoPlayer;

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livevideo);
        superVideoPlayer = getView(R.id.superVideoPlayer);
        superVideoPlayer.setVideoPlayCallback(mVideoPlayCallback);
        url = getIntent().getStringExtra("url");
        if(!TextUtils.isEmpty(url)){
            playVideo();
        }
    }

    private void playVideo() {
        Video video = new Video();
        VideoUrl videoUrl = new VideoUrl();
        videoUrl.setFormatName("480P");
        videoUrl.setFormatUrl(url);
        ArrayList<VideoUrl> arrayList1 = new ArrayList<>();
        arrayList1.add(videoUrl);
        video.setVideoName("");
        video.setVideoUrl(arrayList1);
        video.setmVideoType(VideoType.VIDEO_ONLINE);
        ArrayList<Video> videoArrayList = new ArrayList<>();
        videoArrayList.add(video);
        superVideoPlayer.loadMultipleVideo(videoArrayList, 0, 0, 0);
    }

    private SuperVideoPlayer.VideoPlayCallbackImpl mVideoPlayCallback = new SuperVideoPlayer.VideoPlayCallbackImpl() {
        @Override
        public void onCloseVideo() {
            superVideoPlayer.close();
            superVideoPlayer.setVisibility(View.VISIBLE);
            superVideoPlayer.setVisibility(View.GONE);
            resetPageToPortrait();
        }

        @Override
        public void onSwitchPageType() {
            if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                superVideoPlayer.setPageType(MediaController.PageType.SHRINK);
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                superVideoPlayer.setPageType(MediaController.PageType.EXPAND);
            }
        }

        @Override
        public void onPlayFinish() {

        }
    };


    /***
     * 恢复屏幕至竖屏
     */
    private void resetPageToPortrait() {
        if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            superVideoPlayer.setPageType(MediaController.PageType.SHRINK);
        }
    }

    /***
     * 旋转屏幕之后回调
     *
     * @param newConfig newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (null == superVideoPlayer) return;
        /***
         * 根据屏幕方向重新设置播放器的大小
         */
        float height = DensityUtil.getWidthInPx(this);
        float width = DensityUtil.getHeightInPx(this);
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().getDecorView().invalidate();
            superVideoPlayer.getLayoutParams().height = (int) width;
            superVideoPlayer.getLayoutParams().width = (int) height;
        } else if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            final WindowManager.LayoutParams attrs = getWindow().getAttributes();
            attrs.flags &= (~WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setAttributes(attrs);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            superVideoPlayer.getLayoutParams().height = WindowManager.LayoutParams.MATCH_PARENT;
            superVideoPlayer.getLayoutParams().width = WindowManager.LayoutParams.MATCH_PARENT;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
            resetPageToPortrait();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        superVideoPlayer.pausePlay(true);
    }
}
