package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.NewsAdapter;
import com.iqilu.ksd.adapter.PaikeAdapter;
import com.iqilu.ksd.bean.NewsItemBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.NewsType;
import com.iqilu.ksd.utils.ArrayUtils;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;

/**
 * 精选推荐
 * Created by Coofee on 2017/4/18.
 */

public class RecommendListActivity extends BaseActivity {

    private int page;
    private NewsAdapter adapter;
    private ArrayList<NewsItemBean> list = new ArrayList<NewsItemBean>();

    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private RelativeLayout layLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        adapter = new NewsAdapter(this);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(onItemClickListener);

        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.enableReleaseToLoadMore(true);
        xRefreshView.enableRecyclerViewPullUp(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });
        rvList.setAdapter(adapter);

        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(getString(R.string.rec_title));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    private void getList() {
        String url = Api.URL_ARTICLE_REC + "?page=" + page;
        OkHttpUtils.get().url(url).build().execute(new StringCallback() {

            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
                if (page == 1) {
                    setEmpty();
                }
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<NewsItemBean> data = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<NewsItemBean>>() {
                });
                if (page == 1) {
                    if (data == null || data.size() == 0) {
                        setEmpty();
                        return;
                    }
                    list = data;
                    adapter.setData(list);
                } else {
                    data = ArrayUtils.removeSame(list, data);
                    if (data == null || data.size() == 0) {
                        Toast.makeText(RecommendListActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (list == null) {
                        return;
                    }
                    list.addAll(data);
                    adapter.setData(list);
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    private NewsAdapter.OnItemClickListener onItemClickListener = new NewsAdapter.OnItemClickListener() {
        @Override
        public void OnItemClickListener(View view, int position) {
            Intent intent = null;
            NewsItemBean item = list.get(position);
            if (item.getType().equals(NewsType.HEAD)) {
                item.setType(item.getRealtype());
            }
            if (item.getType().equals(NewsType.ARTICLE)) {
                intent = new Intent(RecommendListActivity.this, NewsDetailActivity.class);
                intent.putExtra("id", item.getId());
                intent.putExtra("catId", item.getCatid());
            } else if (item.getType().equals(NewsType.GALLERY) || item.getType().equals(NewsType.PHOTO)) {
                intent = new Intent(RecommendListActivity.this, GalleryActivity.class);
                intent.putExtra("id", item.getId());
                intent.putExtra("catId", item.getCatid());
                intent.putExtra("position", 0);
            } else if (item.getType().equals(NewsType.AD) || item.getType().equals(NewsType.URL)) {
                intent = new Intent(RecommendListActivity.this, ADActivity.class);
                intent.putExtra("title", item.getTitle());
                intent.putExtra("type", item.getType());
                intent.putExtra("adUrl", item.getUrl());
                if (item.getType().equals(NewsType.AD)) {
                    intent.putExtra("shareicon", item.getShareicon());
                } else {
                    intent.putExtra("thumb", item.getThumb());
                }
            } else if (item.getType().equals(NewsType.LIVE)) {
                intent = new Intent(RecommendListActivity.this, LiveActivity.class);
                intent.putExtra("id", item.getId());
            } else if (item.getType().equals(NewsType.CLUE)) {
                intent = new Intent(RecommendListActivity.this, PaikeActivity.class);
                intent.putExtra("id", item.getId());
            }
            if (intent != null) {
                startActivity(intent);
            }
        }
    };

    private void afterRequest() {
        imgEmpty.setVisibility(View.GONE);
        layLoading.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }

    private void setEmpty() {
        adapter.setData(null);
        adapter.notifyDataSetChanged();
        imgEmpty.setVisibility(View.VISIBLE);
        xRefreshView.setVisibility(View.GONE);
    }
}
