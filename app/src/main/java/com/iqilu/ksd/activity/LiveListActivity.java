package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.LiveAdapter;
import com.iqilu.ksd.bean.LiveBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.JSONUtils;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by Coofee on 2017/4/14.
 */

public class LiveListActivity extends BaseActivity {

    private static String TAG = "LiveListActivity";

    private int page;
    private int groupid;
    private String title;
    private LiveAdapter adapter;
    private ArrayList<LiveBean> list = new ArrayList<LiveBean>();

    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private RelativeLayout layLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        Intent intent = getIntent();
        groupid = intent.getIntExtra("groupid",0);
        title = intent.getStringExtra("title");
        initView();
        adapter = new LiveAdapter(this);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(new LiveAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                LiveBean item =  list.get(position);
                int mId = item.getId();
                int userId = item.getUid();
                String title = item.getTitle();
                String album = item.getLitpic();
                String shareurl = item.getShareurl();
                Intent intent = new Intent(LiveListActivity.this, LiveActivity.class);
                intent.putExtra("id", mId);
                intent.putExtra("userid", userId);
                intent.putExtra("title", title);
                intent.putExtra("album", album);
                intent.putExtra("shareurl", shareurl);
                startActivity(intent);
            }
        });
        rvList.setAdapter(adapter);
        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });
        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        imgEmpty.setImageResource(R.drawable.bg_nolive);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(title);
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    private void getList() {
        String url = Api.URL_LIVE + "?groupid="+groupid + "&page=" + page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
                if(page == 1){
                    xRefreshView.setVisibility(View.GONE);
                    imgEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<LiveBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<LiveBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(LiveListActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        imgEmpty.setVisibility(View.VISIBLE);
                    } else {
                        list = result;
                    }
                } else {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(LiveListActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                    }
                }
                adapter.setData(list);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void afterRequest() {
        layLoading.setVisibility(View.GONE);
        imgEmpty.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }
}
