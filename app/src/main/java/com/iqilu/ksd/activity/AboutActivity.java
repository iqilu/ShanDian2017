package com.iqilu.ksd.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.iqilu.ksd.R;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.BaseUtils;
import com.jaeger.library.StatusBarUtil;

/**
 * Created by Coofee on 2016/10/21.
 */

public class AboutActivity extends com.iqilu.ksd.activity.BaseActivity {

    private TextView tvTitle;
    private ImageView btLeft;
    private TextView tvVersion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        btLeft = getView(R.id.bt_left);
        tvVersion = getView(R.id.tv_version);

        tvTitle.setText(R.string.about_title);
        tvVersion.setText(String.format(getString(R.string.about_version), "" + BaseUtils.getVersion(this)));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
