package com.iqilu.ksd.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.ShakeStatus;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2016/11/22.
 */

public class ShakeResultActivity extends BaseActivity {

    private String title;
    private String secret;
    private String thumb;

    private SimpleDraweeView imgThumb;
    private TextView tvTitle;
    private ImageView btLeft;
    private LinearLayout layNotStart;
    private LinearLayout layEnd;
    private RelativeLayout layInfo;
    private RelativeLayout layResult;
    private LinearLayout layContent;
    private ImageView imgSuccess;
    private TextView tvResult;
    private Button btSubmit;
    private EditText etName;
    private EditText etPhone;
    private EditText etAddr;
    private TextView tvYear;
    private TextView tvHour;

    private LigDialog ligDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shakeresult);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        tvTitle.setText(getString(R.string.shake_lj));
        Intent intent = getIntent();
        title = intent.getStringExtra("title");
        secret = intent.getStringExtra("secret");
        thumb = intent.getStringExtra("thumb");
        imgThumb.setImageURI(Uri.parse(""+thumb));
        tvResult.setText(title);
//        init();
    }

    private void initView(){
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        btLeft = getView(R.id.bt_left);
        imgThumb = getView(R.id.img_thumb);
        layNotStart = getView(R.id.lay_not_start);
        layEnd = getView(R.id.lay_end);
        layInfo = getView(R.id.lay_info);
        layResult = getView(R.id.lay_result);
        layContent = getView(R.id.lay_content);
        imgSuccess = getView(R.id.img_success);
        tvResult = getView(R.id.tv_result);
        btSubmit = getView(R.id.bt_submit);
        etName = getView(R.id.et_name);
        etPhone = getView(R.id.et_phone);
        etAddr = getView(R.id.et_addr);
        tvYear = getView(R.id.tv_year);
        tvHour = getView(R.id.tv_hour);

        setZHTypeface(tvResult);

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btSubmit.getText().equals(getString(R.string.back))) {
                    finish();
                } else {
                    checkInfo();
                }
            }
        });
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back();
            }
        });
    }

//    private void init() {
//        switch (status) {
//            case ShakeStatus.NOT_START:
//                long timeStamp = Integer.valueOf(startTime).longValue() * 1000;
//                SimpleDateFormat format_year = new SimpleDateFormat("yyyy年MM月dd日");
//                SimpleDateFormat format_hour = new SimpleDateFormat("HH:mm:ss");
//                String year = format_year.format(new Date(timeStamp));
//                String hour = format_hour.format(new Date(timeStamp));
//                tvYear.setText(year);
//                tvHour.setText(hour);
//                layNotStart.setVisibility(View.VISIBLE);
//                layEnd.setVisibility(View.GONE);
//                layInfo.setVisibility(View.GONE);
//                imgSuccess.setVisibility(View.GONE);
//                break;
//            case ShakeStatus.END:
//                layNotStart.setVisibility(View.GONE);
//                layEnd.setVisibility(View.VISIBLE);
//                layInfo.setVisibility(View.GONE);
//                imgSuccess.setVisibility(View.GONE);
//                break;
//            case ShakeStatus.WIN:
//                layNotStart.setVisibility(View.GONE);
//                layEnd.setVisibility(View.GONE);
//                layInfo.setVisibility(View.VISIBLE);
//                imgSuccess.setVisibility(View.GONE);
//                tvResult.setText("" + title);
//                break;
//            default:
//                break;
//        }
//    }
    private void checkInfo() {
        String name = etName.getText().toString();
        String phone = etPhone.getText().toString();
        String addr = etAddr.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, R.string.shake_name_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, R.string.shake_phone_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(addr)) {
            Toast.makeText(this, R.string.shake_addr_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        submitInfo(name, phone, addr);
    }

    private void submitInfo(String name, String phone, String addr) {
        String url = Api.URL_ROCK_SAVE;
        OkHttpUtils
                .post()
                .url(url)
                .addParams("name", name)
                .addParams("phone", phone)
                .addParams("addr", addr)
                .addParams("secret", ""+secret)
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        ligDialog = new LigDialog(ShakeResultActivity.this, getString(R.string.waiting));
                        ligDialog.show();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ligDialog.dismiss();
                        Toast.makeText(ShakeResultActivity.this, R.string.shake_save_error, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.i("ttt","response===>"+response);
                        ligDialog.dismiss();
                        int status = 0;
                        status = JSONUtils.filterInt(response,"status",0);
                        if (status == 1) {
//                            layResult.setVisibility(View.GONE);
//                            layContent.setVisibility(View.GONE);
//                            imgSuccess.setVisibility(View.VISIBLE);
//                            btSubmit.setText(getString(R.string.back));
                            Toast.makeText(ShakeResultActivity.this,R.string.shake_save_success,Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(ShakeResultActivity.this, R.string.shake_save_error, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void back(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.shake_back);
        builder.setNegativeButton(R.string.cancel, null);
        builder.setPositiveButton(R.string.ensure, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.show();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        back();
    }
}
