package com.iqilu.ksd.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.GalleryItemBean;
import com.iqilu.ksd.bean.NewsBean;
import com.iqilu.ksd.bean.PaikeBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Request;

/**
 * 查看自己发布的拍客作品
 * <p>
 * Created by Coofee on 2016/12/30.
 */

public class UserPaikeActivity extends BaseActivity {

    private int id;
    private PaikeBean data;
    private MyAdapter adapter;

    private LigDialog ligDialog;

    private ImageView btLeft;
    private TextView tvTitle;
    private TextView tvStatus;
    private TextView tvBody;
    private TextView tvNickname;
    private TextView tvLocation;
    private TextView tvDate;
    private GridView gvList;
    private RelativeLayout layLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userpaike);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        id = getIntent().getIntExtra("id", 0);
        getContent();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        tvStatus = getView(R.id.tv_status);
        tvBody = getView(R.id.tv_body);
        tvNickname = getView(R.id.tv_nickname);
        tvLocation = getView(R.id.tv_location);
        tvDate = getView(R.id.tv_date);
        gvList = getView(R.id.gv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(getString(R.string.pk_my_title));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void toShow() {
        tvStatus.setText(data.getStatustext());
        tvBody.setText(data.getBody());
        tvNickname.setText(data.getNickname());
        tvLocation.setText(data.getLocation());
        tvDate.setText(data.getDate());
        if (data.getImgs() != null) {
            adapter = new MyAdapter();
            gvList.setAdapter(adapter);
            gvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    NewsBean news = new NewsBean();
                    ArrayList<GalleryItemBean> gallery = new ArrayList<GalleryItemBean>();
                    for (String s : data.getImgs()) {
                        GalleryItemBean galleryItemBean = new GalleryItemBean();
                        galleryItemBean.setImg(s);
                        gallery.add(galleryItemBean);
                    }
                    news.setGallery(gallery);
                    news.setTitle(data.getTitle());
                    news.setShareurl(data.getShareurl());
                    news.setDescription(data.getDescription());
                    news.setThumb(data.getThumb());
                    Intent intent = new Intent(UserPaikeActivity.this, GalleryActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("news", news);
                    intent.putExtras(bundle);
                    intent.putExtra("position", Integer.valueOf(position));
                    intent.putExtra("fromLive", true);
                    startActivity(intent);
                }
            });
        }
    }

    private void getContent() {
        String url = Api.URL_CLUE + "?id=" + id;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                ligDialog = new LigDialog(UserPaikeActivity.this, getString(R.string.waiting));
                ligDialog.show();
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                ligDialog.dismiss();
            }

            @Override
            public void onResponse(String response, int id) {
                ligDialog.dismiss();
                data = JSONUtils.requestDetail(response, "data", PaikeBean.class);
                if (data == null) {
                    Toast.makeText(UserPaikeActivity.this, R.string.load_error, Toast.LENGTH_SHORT).show();
                    return;
                }
                toShow();
                layLoading.setVisibility(View.GONE);
            }
        });
    }

    class MyAdapter extends BaseAdapter {

        String[] imgs = data.getImgs();

        @Override
        public int getCount() {
            return imgs == null ? 0 : imgs.length;
        }

        @Override
        public Object getItem(int i) {
            return imgs[i];
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder holder;
            if (view == null) {
                holder = new ViewHolder();
                view = LayoutInflater.from(UserPaikeActivity.this).inflate(R.layout.list_item_pic_paike, null);
                holder.imgThumb = (SimpleDraweeView) view.findViewById(R.id.img_thumb);
                holder.imgDel = (ImageView) view.findViewById(R.id.img_del);
                view.setTag(holder);
            } else {
                holder = (ViewHolder) view.getTag();
            }
            holder.imgThumb.setImageURI(Uri.parse("" + imgs[i]));
            holder.imgDel.setVisibility(View.GONE);
            return view;
        }

        class ViewHolder {
            SimpleDraweeView imgThumb;
            ImageView imgDel;
        }
    }
}
