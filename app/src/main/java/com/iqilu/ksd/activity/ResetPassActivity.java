package com.iqilu.ksd.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.iqilu.ksd.R;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.BaseUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Request;

/**
 * 重置密码
 * <p/>
 * Created by coofee on 2015/11/18.
 */
public class ResetPassActivity extends BaseActivity {

    public static String TAG = "ResetPassActivity";

    private LigDialog ligDialog;
    private String code;
    private String verifycode;
    private String password;

    private ImageView btLeft;
    private EditText etPassword;
    private TextView tvTitle;
    private Button btSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resetpass);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        Intent intent = getIntent();
        code = intent.getStringExtra("code");
        verifycode = intent.getStringExtra("verifycode");
    }

    private void initView() {
        tvTitle = getView(R.id.tv_title);
        tvTitle.setText(R.string.findpass_title);
        setZHTypeface(tvTitle);
        btLeft = getView(R.id.bt_left);
        etPassword = getView(R.id.et_password);
        btSubmit = getView(R.id.bt_submit);
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toSubmit();
            }
        });
    }

    private void toSubmit() {
        password = etPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            Toast.makeText(this, R.string.login_pass_empty, Toast.LENGTH_SHORT).show();
            return;
        }
        if (!BaseUtils.isPassword(password)) {
            Toast.makeText(this, R.string.login_pass_error, Toast.LENGTH_SHORT).show();
            return;
        }
        resetPassword();
    }

    private void resetPassword() {
        String url = Api.URL_RESETPASSWORD;
        OkHttpUtils
                .post()
                .url(url)
                .addParams("code", "" + code)
                .addParams("verifycode", "" + verifycode)
                .addParams("password", "" + password)
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        ligDialog = new LigDialog(ResetPassActivity.this, getString(R.string.waiting));
                        ligDialog.show();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ligDialog.dismiss();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ligDialog.dismiss();
                        int result = 0;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            result = jsonObject.getInt("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (result == 0) {
                            Toast.makeText(ResetPassActivity.this, R.string.findpass_verifycode_over, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(ResetPassActivity.this, R.string.findpass_success, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ResetPassActivity.this, LoginActivity.class));
                            finish();
                        }
                    }
                });

    }
}
