package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.PaikeAdapter;
import com.iqilu.ksd.bean.PaikeBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2016/10/18.
 */

public class MyPaikeActivity extends BaseActivity {

    private static final String TAG = "MyPaikeActivity";

    private int page;
    private ArrayList<PaikeBean> list;
    private PaikeAdapter adapter;

    private LigDialog ligDialog;
    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private RelativeLayout layLoading;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        adapter = new PaikeAdapter(this, true);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(new PaikeAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                Intent intent = new Intent(MyPaikeActivity.this, UserPaikeActivity.class);
                intent.putExtra("id", list.get(position).getId());
                startActivity(intent);
            }
        });
        adapter.setOnDelClickListener(new PaikeAdapter.OnDelClickListener() {
            @Override
            public void OnDelClickListener(View view, final int position) {
                final MaterialDialog dialog = new MaterialDialog(MyPaikeActivity.this);
                dialog.content(MyPaikeActivity.this.getString(R.string.pk_del_des))
                        .btnText(MyPaikeActivity.this.getString(R.string.cancel), MyPaikeActivity.this.getString(R.string.ensure))
                        .show();

                dialog.setOnBtnClickL(
                        new OnBtnClickL() {
                            @Override
                            public void onBtnClick() {
                                dialog.dismiss();
                            }
                        },
                        new OnBtnClickL() {
                            @Override
                            public void onBtnClick() {
                                delPaike(position);
                                dialog.dismiss();
                            }
                        }
                );
            }
        });

        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });
        rvList.setAdapter(adapter);
        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(getString(R.string.pk_my_title));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    private void getList() {
        String url = Api.URL_CLUE_MY + "?page=" + page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                xRefreshView.stopRefresh();
                layLoading.setVisibility(View.GONE);
                if(page == 1){
                    xRefreshView.setVisibility(View.GONE);
                    imgEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onResponse(String response, int id) {
                xRefreshView.stopRefresh();
                layLoading.setVisibility(View.GONE);
                ArrayList<PaikeBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<PaikeBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        imgEmpty.setVisibility(View.VISIBLE);
                        xRefreshView.setVisibility(View.GONE);
                        Toast.makeText(MyPaikeActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list = result;
                        adapter.setData(list);
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    xRefreshView.stopLoadMore();
                    if (result == null || result.size() == 0) {
                        Toast.makeText(MyPaikeActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                        adapter.setData(list);
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });
    }

    public void delPaike(final int position) {
        int id = list.get(position).getId();
        String url = Api.URL_CLUE + "?id=" + id;
        OkHttpUtils.delete().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                ligDialog = new LigDialog(MyPaikeActivity.this, getString(R.string.deleting));
                ligDialog.show();
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                ligDialog.dismiss();
                Toast.makeText(MyPaikeActivity.this, R.string.pk_del_fail, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                ligDialog.dismiss();
                JSONObject jsonObject = null;
                int status = 0;
                try {
                    jsonObject = new JSONObject(response);
                    status = jsonObject.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (status == 1) {
                    list.remove(position);
                    adapter.setData(list);
                    adapter.notifyDataSetChanged();
                    Toast.makeText(MyPaikeActivity.this, R.string.pk_del_success, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyPaikeActivity.this, R.string.pk_del_fail, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
