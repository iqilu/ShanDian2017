package com.iqilu.ksd.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.andview.refreshview.XRefreshView;
import com.andview.refreshview.XRefreshViewFooter;
import com.flyco.dialog.listener.OnBtnClickL;
import com.flyco.dialog.widget.MaterialDialog;
import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.LiveAdapter;
import com.iqilu.ksd.bean.LiveBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.utils.JSONUtils;
import com.iqilu.ksd.widget.LigDialog;
import com.jaeger.library.StatusBarUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2016/10/18.
 */

public class MyLiveActivity extends BaseActivity {

    private int page;
    private ArrayList<LiveBean> list;
    private LiveAdapter adapter;

    private LigDialog ligDialog;
    private ImageView btLeft;
    private TextView tvTitle;
    private ImageView imgEmpty;
    private XRefreshView xRefreshView;
    private RecyclerView rvList;
    private RelativeLayout layLoading;

    private static final int REQUEST_EDIT = 1001;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        StatusBarUtil.setColor(this, ContextCompat.getColor(this, R.color.blue_dark), Config.STATUSBAR_ALPHA);
        initView();
        adapter = new LiveAdapter(this, true);
        adapter.setCustomLoadMoreView(new XRefreshViewFooter(this));
        adapter.setOnItemClickListener(new LiveAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                LiveBean item =  list.get(position);
                int mId = item.getId();
                int userId = item.getUid();
                String title = item.getTitle();
                String album = item.getLitpic();
                String shareurl = item.getShareurl();
                Intent intent = new Intent(MyLiveActivity.this, LiveActivity.class);
                intent.putExtra("id", mId);
                intent.putExtra("userid", userId);
                intent.putExtra("title", title);
                intent.putExtra("album", album);
                intent.putExtra("shareurl", shareurl);
                startActivity(intent);
            }
        });
        rvList.setAdapter(adapter);
        xRefreshView.setPullRefreshEnable(true);
        xRefreshView.setPullLoadEnable(true);
        xRefreshView.setMoveForHorizontal(true);
        xRefreshView.setXRefreshViewListener(new XRefreshView.SimpleXRefreshListener() {
            @Override
            public void onRefresh() {
                super.onRefresh();
                page = 1;
                getList();
            }

            @Override
            public void onLoadMore(boolean isSilence) {
                super.onLoadMore(isSilence);
                page++;
                getList();
            }
        });
        page = 1;
        getList();
    }

    private void initView() {
        btLeft = getView(R.id.bt_left);
        tvTitle = getView(R.id.tv_title);
        setZHTypeface(tvTitle);
        imgEmpty = getView(R.id.img_empty);
        imgEmpty.setImageResource(R.drawable.bg_nolive);
        xRefreshView = getView(R.id.xRefreshView);
        rvList = getView(R.id.rv_list);
        layLoading = getView(R.id.lay_loading);

        tvTitle.setText(getString(R.string.live_my_title));
        btLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        layLoading.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(OrientationHelper.VERTICAL);
        rvList.setLayoutManager(layoutManager);
    }

    public void edit(int position) {
        LiveBean item = list.get(position);
        Intent intent = new Intent(this, AddLiveActivity.class);
        intent.putExtra("id", item.getId());
        intent.putExtra("title", item.getTitle());
        intent.putExtra("litpic", item.getLitpic());
        startActivityForResult(intent, REQUEST_EDIT);
    }

    public void del(final int position) {
        final MaterialDialog dialog = new MaterialDialog(this);
        dialog.content(this.getString(R.string.live_del))
                .btnText(this.getString(R.string.cancel), this.getString(R.string.ensure))
                .show();

        dialog.setOnBtnClickL(
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        dialog.dismiss();
                    }
                },
                new OnBtnClickL() {
                    @Override
                    public void onBtnClick() {
                        delLive(position);
                        dialog.dismiss();
                    }
                }
        );
    }

    private void delLive(final int position) {
        int id = list.get(position).getId();
        String url = Api.URL_LIVE + "?id=" + id;
        OkHttpUtils.delete().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onBefore(Request request, int id) {
                super.onBefore(request, id);
                ligDialog = new LigDialog(MyLiveActivity.this, getString(R.string.deleting));
                ligDialog.show();
            }

            @Override
            public void onError(Call call, Exception e, int id) {
                ligDialog.dismiss();
                Toast.makeText(MyLiveActivity.this, R.string.live_del_fail, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onResponse(String response, int id) {
                int status = 0;
                ligDialog.dismiss();
                status = JSONUtils.filterInt(response, "status", 0);
                if (status == 1) {
                    list.remove(position);
                    adapter.setData(list);
                    adapter.notifyDataSetChanged();
                    if (list == null || list.size() == 0) {
                        imgEmpty.setVisibility(View.VISIBLE);
                    }
                    Toast.makeText(MyLiveActivity.this, R.string.live_del_success, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MyLiveActivity.this, R.string.live_del_fail, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getList() {
        String url = Api.URL_LIVE_MY + "?page=" + page;
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                afterRequest();
                if(page == 1){
                    xRefreshView.setVisibility(View.GONE);
                    imgEmpty.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onResponse(String response, int id) {
                afterRequest();
                ArrayList<LiveBean> result = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<LiveBean>>() {
                });
                if (page == 1) {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(MyLiveActivity.this, R.string.list_empty, Toast.LENGTH_SHORT).show();
                        imgEmpty.setVisibility(View.VISIBLE);
                    } else {
                        list = result;
                    }
                } else {
                    if (result == null || result.size() == 0) {
                        Toast.makeText(MyLiveActivity.this, R.string.list_not_have, Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        list.addAll(result);
                    }
                }
                adapter.setData(list);
                adapter.notifyDataSetChanged();
            }
        });
    }

    private void afterRequest() {
        layLoading.setVisibility(View.GONE);
        imgEmpty.setVisibility(View.GONE);
        if (page == 1) {
            xRefreshView.stopRefresh();
        } else {
            xRefreshView.stopLoadMore();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getList();
    }
}
