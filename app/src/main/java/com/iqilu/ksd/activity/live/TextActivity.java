package com.iqilu.ksd.activity.live;

import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.BaseActivity;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.widget.LigDialog;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;
import okhttp3.Request;

/**
 * Created by Coofee on 2016/11/24.
 */

public class TextActivity extends BaseActivity{

    private static final String TAG = "TextActivity";
    private LigDialog ligDialog;
    private int id;

    private Button btCancel;
    private Button btSend;
    private EditText etEmoji;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_live_view_common);
        initView();
        id = getIntent().getIntExtra("id", 0);
        etEmoji.setFocusable(true);
        etEmoji.setFocusableInTouchMode(true);
        etEmoji.requestFocus();
    }

    private void initView(){
        btCancel = getView(R.id.bt_cancel);
        btSend = getView(R.id.bt_send);
        etEmoji = getView(R.id.et_emoji);

        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    private void save() {
        String comment = etEmoji.getText().toString();
        if (!TextUtils.isEmpty(comment)) {
            addText(comment);
        } else {
            Toast.makeText(this, R.string.live_comment_empty, Toast.LENGTH_SHORT).show();
        }
    }

    private void addText(String comment) {
        String url = Api.URL_LIVE_WORD;
        OkHttpUtils
                .post()
                .url(url)
                .addParams("liveid", "" + id)
                .addParams("body", Uri.encode(comment))
                .tag(this)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onBefore(Request request, int id) {
                        super.onBefore(request, id);
                        ligDialog = new LigDialog(TextActivity.this, getString(R.string.live_send_text));
                        ligDialog.show();
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        ligDialog.dismiss();
                        Toast.makeText(TextActivity.this, R.string.live_addvideo_fail, Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        ligDialog.dismiss();
                        int result = 0;
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            result = jsonObject.getInt("status");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if (result == 1) {
                            Toast.makeText(TextActivity.this, R.string.live_addvideo_success, Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(TextActivity.this, R.string.live_addvideo_fail, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
