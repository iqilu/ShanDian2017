package com.iqilu.ksd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.iqilu.ksd.event.NetworkStateEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by coofee on 2015/12/28.
 */
public class LightingReceiver extends BroadcastReceiver {

    private static String TAG = "KsdReceiver";

    private static final String NET_CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    private static final String WIFI_STATE_CHANGED = "android.net.wifi.WIFI_STATE_CHANGED";

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
//        Log.i(TAG, "action=" + action);
        //检测网络状态
        if(action.equals(NET_CONNECTIVITY_CHANGE) || action.equals(WIFI_STATE_CHANGED)){
//            EventBus.getDefault().post(new NetworkStateEvent());
        }
    }
}
