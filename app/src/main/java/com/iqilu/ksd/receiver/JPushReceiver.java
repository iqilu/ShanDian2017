package com.iqilu.ksd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.iqilu.ksd.activity.ADActivity;
import com.iqilu.ksd.activity.GalleryActivity;
import com.iqilu.ksd.activity.LiveActivity;
import com.iqilu.ksd.activity.MainActivity;
import com.iqilu.ksd.activity.NewsDetailActivity;
import com.iqilu.ksd.constant.NewsType;

import org.json.JSONException;
import org.json.JSONObject;

import cn.jpush.android.api.JPushInterface;

/**
 * JPush推送receiver
 * Created by coofee on 2015/12/10.
 */
public class JPushReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
//            Log.d(TAG, "[MyReceiver] onReceive - " + intent.getAction() + ", extras: " + printBundle(bundle));
//            Intent mIntent = new Intent(context.getApplicationContext(), NewsDetailActivity_.class);
            Intent mIntent = null;
            int id = 0;
            int catid = 0;
            String type = "";
            String url = "";
            JSONObject customJson = null;
            String jsonString = bundle.getString(JPushInterface.EXTRA_EXTRA);
            try {
                customJson = new JSONObject(jsonString);
                type = customJson.getString("type");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (!NewsType.MESSAGE.equals(type)) {
                try {
                    id = customJson.getInt("id");
                    catid = customJson.getInt("catid");
                    url = customJson.getString("url");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            switch (type) {
                case NewsType.ARTICLE:
                    mIntent = new Intent(context.getApplicationContext(), NewsDetailActivity.class);
                    break;
                case NewsType.VIDEO:
//                    mIntent = new Intent(context.getApplicationContext(), VideoDetailActivity.class);
                    break;
                case NewsType.GALLERY:
                    mIntent = new Intent(context.getApplicationContext(), GalleryActivity.class);
                    break;
                case NewsType.URL:
                    mIntent = new Intent(context.getApplicationContext(), ADActivity.class);
                    break;
                case NewsType.MESSAGE:
                    mIntent = new Intent(context.getApplicationContext(), MainActivity.class);
                    break;
                case NewsType.LIVE:
                    mIntent = new Intent(context.getApplicationContext(), LiveActivity.class);
                    break;
                case NewsType.WATCHTV:
                    mIntent = new Intent(context.getApplicationContext(), MainActivity.class);
                    mIntent.putExtra("watchTV", true);
            }
            if (mIntent != null) {
                mIntent.putExtra("fromJPush", true);
                mIntent.putExtra("id", id);
                mIntent.putExtra("catId", catid);
                mIntent.putExtra("adUrl", url);
                mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.getApplicationContext().startActivity(mIntent);
            }
        }
    }

    // 打印所有的 intent extra 数据
    private static String printBundle(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
            } else if (key.equals(JPushInterface.EXTRA_CONNECTION_CHANGE)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getBoolean(key));
            } else {
                sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
            }
        }
        return sb.toString();
    }
}
