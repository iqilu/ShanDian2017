package com.iqilu.ksd.constant;

/**
 * 开始界面广告的一些信息
 * Created by coofee on 2015/12/2.
 */
public class StartAdInfo {

    public static String START_AD_URL = "startadurl";

    public static String START_AD_IMG = "startadimg";

    public static String START_AD_MD5 = "startadmd5";

    public static String START_AD_TITLE = "starttitle";

    public static String START_AD_SHARE_ICON = "startshareicon";
}
