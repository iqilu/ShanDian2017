package com.iqilu.ksd.constant;

/**
 * 通用字段名称
 * Created by coofee on 2015/12/9.
 */
public class Config {

    public static String CHECK_VERSION_TIME = "checkVersionTime"; // 检查更新的时间

    public static String CITY = "city"; // 当前所在城市名称

    public static String CITY_ID = "cityId"; // 当前所在城市id

    public static String SCAN_RESULT = "scanresult"; //二维码扫描结果

    public static String LOGIN_ENCRYPT_KEY = "c0e9fcff59ecc3b8";

    public static String LOGIN_AES_IV = "0000000000000000";

    public static final String WX_APP_ID = "wx63f6a43e83d46802"; //微信支付appId

    public static final String KSDSTATUS = "ksdstatus"; //看电视背景是否替换

    public static final String IS_FIRST = "isFirst";

    public static final String BOX_URL_TOUPIAO = "box_url_toupiao";

    public static final String BOX_URL_BAOMING = "box_url_baoming";

    public static final int STATUSBAR_ALPHA = 30;

    public static final String SHOW_GUIDE = "show_guide";

    public static final String PHONE_UUID = "phone_uuid";

}
