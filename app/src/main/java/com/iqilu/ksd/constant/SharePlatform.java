package com.iqilu.ksd.constant;

/**
 * Created by Coofee on 2016/11/29.
 */

public enum SharePlatform {
    WEIBO,QQ,QZONE,WECHAT,WECHATMOMENTS,SHORTMESSAGE
}
