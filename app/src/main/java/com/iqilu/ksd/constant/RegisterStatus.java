package com.iqilu.ksd.constant;

/**
 * 手机注册返回状态值
 * 0:失败 1：成功 2：已经注册过
 * Created by coofee on 2015/11/14.
 */
public class RegisterStatus {
    public static final int FAIL = 0, SUCESS = 1, HAVE_REGIST = 2;
}
