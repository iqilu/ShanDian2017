package com.iqilu.ksd.constant;

/**
 * 广播直播、点播状态信息
 * Created by coofee on 2015/12/25.
 */
public class RadioInfo {

    //
    public final static String CURRADIOID = "curradioid";

    // Notification
    public final static int ID_NOTIFICATION_SERVICE_RADIO = 5000;

    // 广播
    public final static String SERVICE_MEDIAPLAYER_PLAY = "com.iqilu.lightning.radio.PLAY";
    public final static String SERVICE_MEDIAPLAYER_STOP = "com.iqilu.lightning.radio.STOP";
    public final static String SERVICE_MEDIAPLAYER_LAST = "com.iqilu.lightning.radio.LAST";
    public final static String SERVICE_MEDIAPLAYER_NEXT = "com.iqilu.lightning.radio.NEXT";
    public final static String SERVICE_MEDIAPLAYER_STOPANDCLOSE = "com.iqilu.ksd.radio.STOPANDCLOSE";

    //  状态
    public final static String STATUS_BUFFERING = "buffering";
    public final static String STATUS_PLAY = "play";
    public final static String STATUS_STOP = "stop";
    public final static String STATUS_LAST = "last";
    public final static String STATUS_NEXT = "next";
    public final static String STATUS_FAILED = "failed";
    public final static String STATUS_PREPARED = "prepared";

    // 信息
    public final static String RECEIVER_MEDIAPLAYER_CONTENT = "com.iqilu.lightning.radio.CONTENT";
    public final static String RECEIVER_MEDIAPLAYER_STATUS = "com.iqilu.lightning.radio.STATUS";

    //待删
    public final static String CATNAME = "catname";
    public final static String TITLE = "title";
    public final static String URL = "url";
}
