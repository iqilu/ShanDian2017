package com.iqilu.ksd.constant;

/**
 * Created by coofee on 2015/11/16.
 */
public class UserInfo {
    public static final String USERID = "userId";

    public static final String AVATAR = "avatar";

    public static final String NICKNAME = "nickname";

    public static final String PHONE = "phone";

//    public static final String HASH = "hash";

    public static final String LOGINHASH = "loginhash";

    public static final String SEX = "sex";

    public static final String BIRTHDAY = "birthday";

    public static final String ROLE = "role";

    public static final String PUSHKEY = "pushkey";

    public static final String MSGCOUNT = "msgcount";

}
