package com.iqilu.ksd.constant;

/**
 * 用户身份
 * Created by Coofee on 2016/4/20.
 */
public class UserRole {

    public static final int NORMAL = 0; //未认证

    public static final int AUTH = 1; //已认证

    public static final int REPORTER = 2; //记者
}
