package com.iqilu.ksd.constant;

/**
 * 图片压缩时的参数
 * Created by coofee on 2015/12/10.
 */
public class ImageSize {

    public static int AVATAR_WIDTH = 300;

    public static int AVATAR_HEIGHT = 300;

    public static int BIG_WIDTH = 800;

    public static int BIG_HEIGHT = 1200;

    public static int MIDDLE_WIDTH = 600;

    public static int MIDDLE_HEIGHT = 900;

    public static int SMALL_WIDTH = 300;

    public static int SMALL_HEIGHT = 400;

}
