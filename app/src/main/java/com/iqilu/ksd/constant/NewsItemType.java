package com.iqilu.ksd.constant;

/**
 * Created by Coofee on 2016/4/1.
 */
public class NewsItemType {

    public static final int OTHER = 0;

    public static final int ARTICLE = 1;
    public static final int GALLERY = 2;
    public static final int VIDEO = 3;
    public static final int AD = 4;
    public static final int RADIO = 5;
    public static final int URL = 6;
    public static final int PHOTO = 7;
    public static final int LIVE = 8;
    public static final int CLUE = 9;
    public static final int HEAD = 10;
    public static final int BIG_IMAGE = 11;
}
