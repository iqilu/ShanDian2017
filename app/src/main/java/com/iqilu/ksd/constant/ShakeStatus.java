package com.iqilu.ksd.constant;

/**
 * Created by Coofee on 2016/8/22.
 */
public class ShakeStatus {

    public static final int NOT_START = 0;

    public static final int START = 1;

    public static final int END = 2;

    public static final int WIN = 3;

    public static final int NOT_WIN = 4;
}
