package com.iqilu.ksd.constant;

/**
 * 分享状态
 * Created by coofee on 2015/12/17.
 */
public class ShareStatus {

    public static int FAILE = 0;

    public static int SUCCESS = 1;

    public static int TO_WEIBO = 2; // 跳转至微博分享界面
}
