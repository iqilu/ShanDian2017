package com.iqilu.ksd.constant;

/**
 * Created by Coofee on 2016/10/13.
 */

public class Api {

//    public static final String URL_BASE = "http://sd.iqilu.com/v4/";
    public static final String URL_BASE = "http://ksd.qiludev.com/v5/";

    //认证
    public static final String URL_BASE_AUTH = "https://iuser.iqilu.com/";

    //头条幻灯
    public static final String URL_SLIDE = URL_BASE + "slide";

    //可订阅栏目列表
    public static final String URL_CATE = URL_BASE + "cate";

    //文章列表
    public static final String URL_ARTICLE_LIST = URL_BASE + "article/list";

    //文章详情
    public static final String URL_ARTICLE = URL_BASE + "article";

    //精选推荐
    public static final String URL_ARTICLE_REC = URL_BASE + "article/jingxuan";

    //热门推荐
    public static final String URL_ARTICLE_HOT = URL_BASE + "article/hot";

    //视频详情
    public static final String URL_VIDEO = URL_BASE + "video";

    //视频列表
    public static final String URL_VIDEO_LIST = URL_BASE + "video/list";

    //用户注册
    public static final String URL_USER = URL_BASE + "user?version=2";

    //用户登录
    public static final String URL_USER_LOGIN = URL_BASE + "user/login";

    //hash登录
    public static final String URL_USER_HASHLOGIN = URL_BASE + "user/hashLogin";

    //第三方登录
    public static final String URL_USER_THIRTYLOGIN = URL_BASE + "user/thirtylogin";

    //修改资料
    public static final String URL_USER_PUT = URL_BASE + "user/put";

    //上传头像
    public static final String URL_USER_AVATAR = URL_BASE + "user/avatar";

    //设置当前用户的订阅栏目
    public static final String URL_SUBSCRIBE = URL_BASE + "subscribe";

    //验证手机号
    public static final String URL_CHECKPHONECODE = URL_BASE + "user/checkphonecode";

    //设置新密码
    public static final String URL_RESETPASSWORD = URL_BASE + "user/resetpassword";

    //我订阅的列表
    public static final String URL_SBSCRIBE_MY = URL_BASE + "subscribe/my";

    //订阅栏目内容列表
    public static final String URL_SUBSCRIBE_CATE = URL_BASE + "subscribe/cate";

    //添加爆料/我的爆料详情
    public static final String URL_CLUE = URL_BASE + "clue";

    //公共爆料详情
    public static final String URL_CLUE_SHOW = URL_BASE + "clue/show";

    //我的爆料列表
    public static final String URL_CLUE_MY = URL_BASE + "clue/my";

    //爆料列表
    public static final String URL_CLUE_LIST = URL_BASE + "clue/list";

    //收藏
    public static final String URL_FAVORITE = URL_BASE + "favorite";

    //点赞
    public static final String URL_ARTICLE_LIKE = URL_BASE + "article/like";

    //检查新版本
    public static final String URL_VERSION = URL_BASE + "version";

    //启动界面广告
    public static final String URL_ADS_LAUNCH = URL_BASE + "ads/launch";

    //栏目推荐（电视）
    public static final String URL_VIDEO_RECOMMEND = URL_BASE + "video/recommend";

    //栏目推荐（广播）
    public static final String URL_RADIO_RECOMMEND = URL_BASE + "radio/recommend";

    //评论
    public static final String URL_COMMENT = URL_BASE + "comment";

    //直播的评论
    public static final String URL_COMMENT_BYRANGE = URL_BASE + "comment/byRange";

    //退出登录
    public static final String URL_LOGOUT = URL_BASE + "user/logout";

    //广播详情
    public static final String URL_RADIO = URL_BASE + "radio";

    //广播列表
    public static final String URL_RADIO_LIST = URL_BASE + "radio/list";

    //搜索
    public static final String URL_ARTICLE_SEARCH = URL_BASE + "article/search";

    //直播列表/添加直播/删除直播
    public static final String URL_LIVE = URL_BASE + "live";

    //编辑直播
    public static final String URL_LIVE_EDIT = URL_BASE + "live/edit";

    //直播详情
    public static final String URL_LIVE_DETAIL = URL_BASE + "live/detail";

    //我的直播列表
    public static final String URL_LIVE_MY = URL_BASE + "live/my";

    //直播详情列表
    public static final String URL_LIVE_DETAILLIST = URL_BASE + "live/detailListByRange";

    //置顶直播详情列表
    public static final String URL_LIVE_DETAILTOPLIST = URL_BASE + "live/detailtoplist";

    //编辑直播详情文字信息
    public static final String URL_LIVE_EDITDETAIL = URL_BASE + "live/editdetail";

    //我参与的直播列表
    public static final String URL_LIVE_MYJOIN = URL_BASE + "live/myJoin";

    //我的消息列表
    public static final String URL_MSG = URL_BASE + "msg";

    //所有消息设为已读
    public static final String URL_MSG_ALL = URL_BASE + "msg/all";

    //清空消息
    public static final String URL_MSG_CLEAR = URL_BASE +"msg/clear";

    //是否有未读消息
    public static final String URL_MSG_HASUNREAD = URL_BASE + "msg/hasunread";

    //获取当前验证状态 / 提交验证信息
    public static final String URL_IDENTITY = URL_BASE + "identity";

    //获取直播默认封面
    public static final String URL_LIVE_DEFAULTIMAGE = URL_BASE + "live/defaultimage";

    //直播发布文字
    public static final String URL_LIVE_WORD = URL_BASE + "live/word";

    //直播发布视频
    public static final String URL_LIVE_VIDEO = URL_BASE + "live/video";

    //直播发布图片
    public static final String URL_LIVE_IMAGE = URL_BASE + "live/image";

    //直播发布音频
    public static final String URL_LIVE_AUDIO = URL_BASE + "live/audio";

    //直播条
    public static final String URL_LIVE_BAR = URL_BASE + "live/zhibotiao";

    //扫码认证登录
    public static final String URL_SCAN_LOGIN = URL_BASE_AUTH + "auth/scanlogin";

    //获取微信订单
    public static final String URL_ORDER_WEIXIN = URL_BASE_AUTH + "pay/mobile/weixin";

    //摇一摇
    public static final String URL_ROCK = URL_BASE + "rock";

    //摇一摇
    public static final String URL_ROCK_INFO = URL_BASE + "rock/info";

    //中奖后保存邮寄信息
    public static final String URL_ROCK_SAVE = URL_BASE + "rock/save";

    //电视频道列表
    public static final String URL_VIDEO_CHANNEL = URL_BASE+"program/video/channel/new";

    //电视节目单
    public static final String URL_PROGRAM_VIDEO = URL_BASE + "program/video";

    //广播频道列表
    public static final String URL_RADIO_CHANNEL = URL_BASE+"program/radio/channel";

    //广播节目单
    public static final String URL_PROGRAM_RADIO = URL_BASE+"program/radio";

    //电视节目单（新）
    public static final String URL_CATE_TV = URL_BASE + "cate/tv";

    //广播节目单（新）
    public static final String URL_CATE_RADIO = URL_BASE + "cate/radio";

    //投票报名链接
//    public static final String URL_TOUPIAO_BAOMING = URL_BASE + "toupiaobaoming";

    //投票
    public static final String URL_BMTP_TOUPIAO = URL_BASE + "bmtp/toupiao";

    //报名
    public static final String URL_BMTP_BAOMING = URL_BASE + "bmtp/baoming";

    //绑定手机
    public static final String URL_USER_BINDPHONE = URL_BASE + "user/bindPhone";

    //摇一摇列表
    public static final String URL_ROCK_LIST = URL_BASE + "rock/list";

    //获奖名单
    public static final String URL_ROCK_WINNER = URL_BASE + "rock/winner";
}
