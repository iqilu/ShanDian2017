package com.iqilu.ksd.constant;

/**
 * 新闻类型
 *
 * Created by coofee on 2015/11/5.
 */
public class NewsType {
    public static final String HEAD = "head";
    public static final String ARTICLE = "article";
    public static final String GALLERY = "gallery";
    public static final String VIDEO = "video";
    public static final String RADIO = "radio";
    public static final String AD = "ad";
    public static final String URL = "url";
    public static final String PHOTO = "photo";
    public static final String MESSAGE = "message";
    public static final String LIVE = "live";
    public static final String CLUE = "clue";
    public static final String WATCHTV = "watchtv";
}
