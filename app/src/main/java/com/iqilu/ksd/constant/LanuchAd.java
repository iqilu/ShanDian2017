package com.iqilu.ksd.constant;

/**
 * 开始界面广告的一些信息
 * Created by coofee on 2015/12/2.
 */
public class LanuchAd {

    public static String URL = "startadurl";

    public static String IMG = "startadimg";

    public static String MD5 = "startadmd5";

    public static String TITLE = "starttitle";

    public static String SHARE_ICON = "startshareicon";
}
