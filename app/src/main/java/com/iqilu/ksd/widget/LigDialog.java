package com.iqilu.ksd.widget;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import com.iqilu.ksd.R;

/**
 * Created by Coofee on 2016/10/21.
 */

public class LigDialog extends AlertDialog {

    private Context context;
    private String message;
    private TextView tvMessage;

    public LigDialog(Context context) {
        super(context);
        this.context = context;
    }

    public LigDialog(Context context, String message) {
        super(context);
        this.context = context;
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.widget_ligdialog);
        tvMessage = (TextView) findViewById(R.id.tv_message);
        if (message != null && !TextUtils.isEmpty(message)) {
            tvMessage.setText(message);
        }
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

}
