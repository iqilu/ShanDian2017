
package com.iqilu.ksd;

import android.support.multidex.MultiDexApplication;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.iqilu.ksd.bean.UserBean;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.https.HttpsUtils;

import java.util.concurrent.TimeUnit;

import cn.jpush.android.api.JPushInterface;
import okhttp3.OkHttpClient;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Coofee on 2016/9/30.
 */

public class MyApplication extends MultiDexApplication {

    private static final String TAG = "MyApplication";

    private UserBean user;
    private Boolean mainStarted = false;
    private ClearableCookieJar cookieJar;

    @Override
    public void onCreate() {
        super.onCreate();
//        initHotfix();
        initFonts();
        initOkhttp();
        Fresco.initialize(this);
        JPushInterface.init(this);
        JPushInterface.setDebugMode(false);
    }

    private void initFonts() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/FZLTXHK.TTF")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    private void initOkhttp() {
//        HttpsUtils.SSLParams sslParams = HttpsUtils.getSslSocketFactory(getAssets().open(), null, null);
        cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
//          .addInterceptor(new LoggerInterceptor("TAG"))
                .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                .readTimeout(10000L, TimeUnit.MILLISECONDS)
                .cookieJar(cookieJar)
                .build();
        OkHttpUtils.initClient(okHttpClient);
    }

//    private void initHotfix() {
//        String appVersion;
//        try {
//            appVersion = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
//        } catch (Exception e) {
//            appVersion = "1.0.0";
//        }
//        SophixManager.getInstance()
//                .setContext(this)
//                .setAppVersion(appVersion)
//                .setAesKey(null)
//                .setEnableDebug(true)
//                .setPatchLoadStatusStub(new PatchLoadStatusListener() {
//                    @Override
//                    public void onLoad(final int mode, final int code, final String info, final int handlePatchVersion) {
//                        String msg = new StringBuilder("").append("Mode:").append(mode)
//                                .append(" Code:").append(code)
//                                .append(" Info:").append(info)
//                                .append(" HandlePatchVersion:").append(handlePatchVersion).toString();
//                        Log.i("ttt","msg====>"+msg);
//
//                    }})
//                .initialize();
//        SophixManager.getInstance().queryAndLoadNewPatch();
//    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public Boolean getMainStarted() {
        return mainStarted;
    }

    public void setMainStarted(Boolean mainStarted) {
        this.mainStarted = mainStarted;
    }

    public void clearCookie() {
        if (cookieJar != null) {
            cookieJar.clear();
        }
    }

    public ClearableCookieJar getCookieJar(){
        return cookieJar;
    }
}
