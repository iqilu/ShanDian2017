package com.iqilu.ksd.view;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.BannerBean;
import com.iqilu.ksd.constant.NewsType;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;

/**
 * 幻灯view
 * Created by Coofee on 2016/10/13.
 */

public class BannerView extends RelativeLayout {

    private Context context;
    private ArrayList<BannerBean> data;
    private Boolean canSlide = true; //当前是否能够滑动
    private Boolean showLocation = false; //是否显示切换城市按钮
    private ImageView[] mDots;
//    private SimpleDraweeView[] mImageViews;
    private int mHeight = 0;

    private RelativeLayout layMain;
    private ViewPager vpContent;
    private ImageView imgIcon;
    private CircleIndicator layIndicator;
    private TextView tvTitle;
    private ImageView btLocation;

    public BannerView(Context context, ArrayList<BannerBean> data) {
        super(context);
        this.context = context;
        this.data = data;
        bind();
    }

    public BannerView(Context context, ArrayList<BannerBean> data,Boolean showLocation) {
        super(context);
        this.context = context;
        this.data = data;
        this.showLocation = showLocation;
        bind();
    }

    public BannerView(Context context, ArrayList<BannerBean> data,Boolean showLocation,int height) {
        super(context);
        this.context = context;
        this.data = data;
        this.showLocation = showLocation;
        this.mHeight = height;
        bind();
    }

    public void bind() {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.view_banner,null);
        layMain = (RelativeLayout) layout.findViewById(R.id.lay_main);
        vpContent = (ViewPager) layout.findViewById(R.id.vp_content);
        imgIcon = (ImageView) layout.findViewById(R.id.img_icon);
        layIndicator = (CircleIndicator) layout.findViewById(R.id.lay_indicator);
        tvTitle = (TextView) layout.findViewById(R.id.tv_title);
        btLocation = (ImageView) layout.findViewById(R.id.bt_location);
//        layIndicator = (CircleIndicator) layout.findViewById(R.id.lay_indicator);
        if(mHeight > 0){
            ViewGroup.LayoutParams params = layMain.getLayoutParams();
            params.height = mHeight;
            layMain.setLayoutParams(params);
        }

//        mImageViews = new SimpleDraweeView[data.size()];
        mDots = new ImageView[data.size()];
//        for (int i = 0; i < mImageViews.length; i++) {
//            SimpleDraweeView imageView = new SimpleDraweeView(context);
//            imageView.setImageURI(Uri.parse(data.get(i).getImg()));
//            mImageViews[i] = imageView;
//        }

        vpContent.setAdapter(new MyAdapter());
        vpContent.setCurrentItem(data.size() * 100);
        setTitle(data.get(0));
        vpContent.addOnPageChangeListener(pageChangeListener);
        vpContent.setOnTouchListener(touchListener);
        btLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                context.startActivity(new Intent(context, SelectCityActivity_.class));
            }
        });
        if (showLocation) {
            btLocation.setVisibility(VISIBLE);
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (canSlide) {
                        handler.sendEmptyMessage(new Message().what);
                    }
                }
            }
        }).start();

        layIndicator.setViewPager(vpContent,data.size());

        this.addView(layout);
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            vpContent.setCurrentItem((vpContent.getCurrentItem() + 1));
        }
    };

    OnTouchListener touchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            switch (motionEvent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    canSlide = false;
                    break;
                case MotionEvent.ACTION_UP:
                    canSlide = true;
                    break;
            }
            return false;
        }
    };

    ViewPager.OnPageChangeListener pageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            int mPositin = position % data.size();
            setTitle(data.get(mPositin));
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public void setTitle(BannerBean bean) {
        tvTitle.setText(bean.getTitle());
        if (bean.getType().equals(NewsType.GALLERY)) {
            imgIcon.setImageResource(R.drawable.ic_banner_pic);
        } else if (bean.getType().equals(NewsType.VIDEO)) {
            imgIcon.setImageResource(R.drawable.ic_banner_video);
        } else if(bean.getType().equals(NewsType.LIVE)){
            imgIcon.setImageResource(R.drawable.ic_banner_live);
        } else {
            imgIcon.setImageResource(R.drawable.ic_banner_news);
        }
    }

    public class MyAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return data.size() * 1000000;
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView(mImageViews[position % mImageViews.length]);
            container.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            SimpleDraweeView image = new SimpleDraweeView(context);
            image.setImageURI(Uri.parse(data.get(position % data.size()).getImg()));
            final BannerBean bean = data.get(position % data.size());
//            mImage.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Intent intent = null;
//                    if (bean.getType().equals(NewsType.ARTICLE)) { // 新闻
//                        intent = new Intent(context, NewsDetailActivity_.class);
//                    } else if (bean.getType().equals(NewsType.AD) || bean.getType().equals(NewsType.URL)) { // 广告 / 链接
//                        intent = new Intent(getContext(), AdActivity_.class);
//                    } else if (bean.getType().equals(NewsType.GALLERY)) { // 图集
//                        intent = new Intent(getContext(), GalleryActivity_.class);
//                    } else if (bean.getType().equals(NewsType.VIDEO)) { // 视频
//                        intent = new Intent(getContext(), VideoDetailActivity_.class);
//                    } else if (bean.getType().equals(NewsType.RADIO)) { // 音频
//                        intent = new Intent(getContext(), PlayRadioActivity_.class);
//                    } else if(bean.getType().equals(NewsType.LIVE)){ //直播
//                        intent = new Intent(getContext(), LiveActivity_.class);
//                    }
//                    if (intent != null) {
//                        if (bean.getType().equals(NewsType.AD) || bean.getType().equals(NewsType.URL)) {
//                            intent.putExtra("adUrl", bean.getCon());
//                        } else {
//                            intent.putExtra("id", Integer.valueOf(bean.getCon()));
//                        }
//                        intent.putExtra("catId", Integer.valueOf(bean.getCatId()));
//                        context.startActivity(intent);
//                    }
//                }
//            });
            image.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Toast.makeText(context,bean.getTitle(),Toast.LENGTH_SHORT).show();
                    Toast.makeText(context,bean.getTitle(),Toast.LENGTH_SHORT).show();
                }
            });
            container.addView(image);
            return image;
        }
    }

}
