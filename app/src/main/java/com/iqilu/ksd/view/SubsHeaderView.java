package com.iqilu.ksd.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.iqilu.ksd.R;

/**
 * Created by Coofee on 2016/12/2.
 */

public class SubsHeaderView extends RelativeLayout {

    private Context context;

    private TextView tvCount;
    private RelativeLayout layAdd;

    public SubsHeaderView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public SubsHeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.view_subshead, null);
        tvCount = (TextView) layout.findViewById(R.id.tv_count);
        layAdd = (RelativeLayout) layout.findViewById(R.id.lay_add);
//        layAdd.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                context.startActivity(new Intent(context, AddSubscribeActivity.class));
//            }
//        });
        this.addView(layout);
    }

    public void setData(int count) {
        tvCount.setText(String.format(context.getString(R.string.my_column_num), "" + count));
    }
}
