package com.iqilu.ksd.view;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.NewsItemBean;

/**
 * Created by Coofee on 2016/11/2.
 */

public class GalleryView extends RelativeLayout {

    private Context context;
    private NewsItemBean data;

    private TextView tvTitle;
    private SimpleDraweeView itemImg_0;
    private SimpleDraweeView itemImg_1;
    private SimpleDraweeView itemImg_2;

    public GalleryView(Context context, NewsItemBean data) {
        super(context);
        this.context = context;
        this.data = data;
        bind();
    }

    public void bind(){
        RelativeLayout layout = (RelativeLayout) LayoutInflater.from(context).inflate(R.layout.list_item_gallery,null);
        tvTitle = (TextView) layout.findViewById(R.id.tv_title);
        itemImg_0 = (SimpleDraweeView) layout.findViewById(R.id.item_img_0);
        itemImg_1 = (SimpleDraweeView) layout.findViewById(R.id.item_img_1);
        itemImg_2 = (SimpleDraweeView) layout.findViewById(R.id.item_img_2);
        tvTitle.setText(data.getTitle());
        itemImg_0.setImageURI(Uri.parse("" + data.getGallery().get(0)));
        if (data.getGallery().size() > 1) {
            itemImg_1.setImageURI(Uri.parse("" + data.getGallery().get(1)));
        }
        if (data.getGallery().size() > 2) {
            itemImg_2.setImageURI(Uri.parse("" + data.getGallery().get(2)));
        }
        this.addView(layout);
    }
}
