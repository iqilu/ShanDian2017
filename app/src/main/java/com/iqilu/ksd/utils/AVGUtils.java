package com.iqilu.ksd.utils;

import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;

/**
 * Created by Coofee on 2017/1/16.
 */

public class AVGUtils {

    public static String getBuildVersion() {
        return "" + Build.VERSION.SDK_INT;
    }

    public static String getDeviceID(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        return "" + telephonyManager.getDeviceId();
    }
}
