package com.iqilu.ksd.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.WeiboActivity;
import com.iqilu.ksd.constant.SharePlatform;
import com.iqilu.ksd.constant.ShareStatus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.system.text.ShortMessage;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import cn.sharesdk.wechat.moments.WechatMoments;

/**
 * 分享界面
 * Created by Coofee on 2016/3/1
 */
public class ShareUtils {

    private static ShareUtils instance = null;

    private Context context;
    private Platform.ShareParams sp;
    private Platform platform = null;
    private AlertDialog dialog;
    private GridView gridView;
    private RelativeLayout cancelButton;
    private SimpleAdapter saImageItems;
    private int[] image = {R.drawable.bt_share_wechat, R.drawable.bt_share_wechatmoments,R.drawable.bt_share_sinaweibo, R.drawable.bt_share_qq};
    private String[] name = {"微信", "朋友圈","微博", "QQ"};

    private ShareUtils() {
    }

    public static ShareUtils getInstance() {
        if (instance == null) {
            instance = new ShareUtils();
        }
        return instance;
    }

    public ShareUtils setShareParams(Context context, Platform.ShareParams sp) {
        this.context = context;
        this.sp = sp;
        return this;
    }

    public void show() {
//        ShareSDK.initSDK(context);
        dialog = new AlertDialog.Builder(context).create();
        dialog.show();
        Window window = dialog.getWindow();
        window.setContentView(R.layout.popu_share_dialog);
        gridView = (GridView) window.findViewById(R.id.share_gridView);
        cancelButton = (RelativeLayout) window.findViewById(R.id.share_cancel);
        List<HashMap<String, Object>> shareList = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < image.length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("img_thumb", image[i]);
            map.put("txt_title", name[i]);
            shareList.add(map);
        }
        saImageItems = new SimpleAdapter(context, shareList, R.layout.list_item_share, new String[]{"img_thumb", "txt_title"}, new int[]{R.id.img_thumb, R.id.tv_title});
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        gridView.setAdapter(saImageItems);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                platform = null;
                SharePlatform sharePlatform = SharePlatform.WEIBO;
                switch (position) {
                    case 0:
                        sharePlatform = SharePlatform.WECHAT;
                        break;
                    case 1:
                        sharePlatform = SharePlatform.WECHATMOMENTS;
                        break;
                    case 2:
                        sharePlatform = SharePlatform.WEIBO;
                        break;
                    case 3:
                        sharePlatform = SharePlatform.QQ;
                        break;
                }
                toShare(sharePlatform);
                dialog.dismiss();
            }
        });
    }

    public void toShare(SharePlatform sharePlatform) {
        platform = null;
        switch (sharePlatform) {
            case WEIBO:
                break;
            case QQ:
                platform = ShareSDK.getPlatform(QQ.NAME);
                break;
            case QZONE:
                platform = ShareSDK.getPlatform(QZone.NAME);
                break;
            case WECHAT:
                platform = ShareSDK.getPlatform(context, Wechat.NAME);
                sp.setShareType(Platform.SHARE_WEBPAGE);
                break;
            case WECHATMOMENTS:
                platform = ShareSDK.getPlatform(context, WechatMoments.NAME);
                sp.setShareType(Platform.SHARE_WEBPAGE);
                break;
            case SHORTMESSAGE:
                platform = ShareSDK.getPlatform(ShortMessage.NAME);
                break;
            default:
                break;
        }
        if (platform != null) {
            platform.setPlatformActionListener(new PlatformActionListener() {
                @Override
                public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                    handler.sendEmptyMessage(ShareStatus.SUCCESS);
                }

                @Override
                public void onError(Platform platform, int i, Throwable throwable) {
                    handler.sendEmptyMessage(ShareStatus.FAILE);
                }

                @Override
                public void onCancel(Platform platform, int i) {

                }
            });
            // 执行分享
            platform.share(sp);
        } else {
            handler.sendEmptyMessage(ShareStatus.TO_WEIBO);
        }
    }

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == ShareStatus.SUCCESS) {
                Toast.makeText(context, R.string.share_success, Toast.LENGTH_SHORT).show();
            } else if (msg.what == ShareStatus.TO_WEIBO) {
                jumpToWeibo();
            } else {
                Toast.makeText(context, R.string.share_error, Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void jumpToWeibo() {
        Intent intent = new Intent(context, WeiboActivity.class);
        intent.putExtra("title", sp.getTitle());
        intent.putExtra("shareUrl", sp.getUrl());
        intent.putExtra("thumb", sp.getImageUrl());
        context.startActivity(intent);
    }
}
