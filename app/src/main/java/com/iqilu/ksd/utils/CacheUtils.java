package com.iqilu.ksd.utils;

import android.content.Context;

import java.io.File;

/**
 * Created by Coofee on 2016/10/13.
 */

public class CacheUtils {
    public static String getCacheDir(Context context) {
        File cacheDir = null;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir = context.getExternalCacheDir();
        } else {
            cacheDir = context.getCacheDir();
        }
        return cacheDir.getAbsolutePath() + "/";
    }

    public static String getVideoDir(Context context) {
        return getCacheDir(context) + "video/";
    }

    public static String getPicDir(Context context) {
        return getCacheDir(context) + "picture/";
    }

    public static String getVoiceDir(Context context) {
        return getCacheDir(context) + "voice/";
    }

    public static String getCompressDir(Context context) {
        return getCacheDir(context) + "compress/";
    }
}
