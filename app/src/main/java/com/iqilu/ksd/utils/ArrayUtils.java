package com.iqilu.ksd.utils;


import com.iqilu.ksd.bean.LiveItemBean;
import com.iqilu.ksd.bean.NewsItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/5/26.
 */
public class ArrayUtils {

    /**
     * 去除新闻列表中的重复项
     * @param oldList
     * @param newList
     * @return
     */
    public static ArrayList<NewsItemBean> removeSame(ArrayList<NewsItemBean> oldList, ArrayList<NewsItemBean> newList){

        ArrayList<NewsItemBean> result = new ArrayList<NewsItemBean>();

        if(oldList == null || oldList.size() ==0 || newList == null || newList.size() ==0) return newList;
        for (int i=0;i<newList.size();i++){
            for (int j=0;j<oldList.size();j++){
                if((newList.get(i).getId() == oldList.get(j).getId()) && oldList.get(j).getId() != 0){
                    newList.remove(i);
                    i--;
                    break;
                }
            }
        }
        return newList;
    }

    /**
     * 去除直播列表中的重复项
     * @param oldList
     * @param newList
     * @return
     */
    public static ArrayList<LiveItemBean> removeLiveSame(ArrayList<LiveItemBean> oldList, ArrayList<LiveItemBean> newList){

        ArrayList<NewsItemBean> result = new ArrayList<NewsItemBean>();

        if(oldList == null || oldList.size() ==0 || newList == null || newList.size() ==0) return newList;
        for (int i=0;i<newList.size();i++){
            for (int j=0;j<oldList.size();j++){
                if(newList.get(i).getId() == oldList.get(j).getId()){
                    newList.remove(i);
                    i--;
                    break;
                }
            }
        }
        return newList;
    }
}
