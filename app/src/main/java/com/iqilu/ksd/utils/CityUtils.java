package com.iqilu.ksd.utils;

import com.iqilu.ksd.bean.CityItemBean;

import java.util.ArrayList;

public class CityUtils {

    public static ArrayList<CityItemBean> cityList;

    static {
        cityList = new ArrayList<CityItemBean>();
        cityList.add(new CityItemBean(8, "济南"));
        cityList.add(new CityItemBean(9, "青岛"));
        cityList.add(new CityItemBean(10, "淄博"));
        cityList.add(new CityItemBean(11, "东营"));
        cityList.add(new CityItemBean(12, "烟台"));
        cityList.add(new CityItemBean(13, "潍坊"));
        cityList.add(new CityItemBean(14, "济宁"));
        cityList.add(new CityItemBean(15, "泰安"));
        cityList.add(new CityItemBean(16, "威海"));
        cityList.add(new CityItemBean(17, "日照"));
        cityList.add(new CityItemBean(18, "莱芜"));
        cityList.add(new CityItemBean(19, "临沂"));
        cityList.add(new CityItemBean(20, "德州"));
        cityList.add(new CityItemBean(21, "聊城"));
        cityList.add(new CityItemBean(22, "滨州"));
        cityList.add(new CityItemBean(23, "枣庄"));
        cityList.add(new CityItemBean(24, "菏泽"));


    }

    /**
     * 获取城市列表
     *
     * @return
     */
    public static ArrayList<CityItemBean> getCityList() {
        return cityList;
    }

    /**
     * 获取前台显示的城市
     *
     * @param currCity
     * @return
     */
    public static CityItemBean getCity(String currCity) {
        currCity = currCity.substring(0, currCity.length() - 1);
        for (int i = 0; i < cityList.size(); i++) {
            if (cityList.get(i).getCityName().equals(currCity)) {
                return cityList.get(i);
            }
        }
        return cityList.get(0);
    }

}
