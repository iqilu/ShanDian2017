package com.iqilu.ksd.utils;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.TVBean;

import java.util.ArrayList;

/**
 * 获取广播、电视直播列表
 * Created by coofee on 2015/12/3.
 */
public class TVUtils {

    private static ArrayList<TVBean> tvList;

    private static ArrayList<TVBean> radioList;

    //    private static String tvUrl = "http://newlive.iqilu.com:1935/live/";
    private static String tvUrl = "http://ksdlive.iqilu.com/live2/";

    private static String radioUrl = "http://newlive.iqilu.com:1935/ksd_radio/";

    static {
        tvList = new ArrayList<TVBean>();
        tvList.add(new TVBean(24, R.string.tv_weishi, R.drawable.cover_ws,tvUrl + "sdtv.m3u8", "/live2/sdtv.m3u8"));
        tvList.add(new TVBean(25, R.string.tv_qilu, R.drawable.cover_ql, tvUrl + "qlpd.m3u8", "/live2/qlpd.m3u8"));
        tvList.add(new TVBean(26, R.string.tv_tiyu, R.drawable.cover_ty, tvUrl + "typdnologo.m3u8", "/live2/typdnologo.m3u8"));
        tvList.add(new TVBean(29, R.string.tv_shenghuo, R.drawable.cover_sh, tvUrl + "shpd.m3u8", "/live2/shpd.m3u8"));
        tvList.add(new TVBean(28, R.string.tv_zongyi, R.drawable.cover_zy, tvUrl + "zypd.m3u8", "/live2/zypd.m3u8"));
        tvList.add(new TVBean(27, R.string.tv_yingshi, R.drawable.cover_ys, tvUrl + "yspd.m3u8", "/live2/yspd.m3u8"));
        tvList.add(new TVBean(31, R.string.tv_gonggong, R.drawable.cover_gg, tvUrl + "ggpd.m3u8", "/live2/ggpd.m3u8"));
        tvList.add(new TVBean(30, R.string.tv_nongke, R.drawable.cover_nk, tvUrl + "nkpd.m3u8", "/live2/nkpd.m3u8"));
        tvList.add(new TVBean(32, R.string.tv_shaoer, R.drawable.cover_se, tvUrl + "sepd.m3u8", "/live2/sepd.m3u8"));
        tvList.add(new TVBean(33, R.string.tv_guoji, R.drawable.cover_gj, tvUrl + "gjpd.m3u8", "/live2/gjpd.m3u8"));
        tvList.add(new TVBean(62, R.string.tv_jujia, R.drawable.cover_gj, "http://ksdlive.gwpd.iqilu.com/gwpd.m3u8", "/live2/gwpd.m3u8"));
    }

    static {
        radioList = new ArrayList<TVBean>();
        radioList.add(new TVBean(1, R.string.radio_news, R.drawable.cover_renmintai, radioUrl + "sdradio01/playlist.m3u8",""));
        radioList.add(new TVBean(2, R.string.radio_jingji, R.drawable.cover_jingji, radioUrl + "sdradio02/playlist.m3u8",""));
        radioList.add(new TVBean(3, R.string.radio_nvzhubo, R.drawable.cover_nvzhubo, radioUrl + "sdradio03/playlist.m3u8",""));
        radioList.add(new TVBean(4, R.string.radio_shenghuo, R.drawable.cover_shenghuo, radioUrl + "sdradio04/playlist.m3u8",""));
        radioList.add(new TVBean(5, R.string.radio_jiaotong, R.drawable.cover_jiaotong, radioUrl + "sdradio05/playlist.m3u8",""));
        radioList.add(new TVBean(6, R.string.radio_xiangcun, R.drawable.cover_xiangcun, radioUrl + "sdradio06/playlist.m3u8",""));
        radioList.add(new TVBean(7, R.string.radio_yinyue, R.drawable.cover_yinyue, radioUrl + "sdradio07/playlist.m3u8",""));
        radioList.add(new TVBean(8, R.string.radio_tiyu, R.drawable.cover_tiyu, radioUrl + "sdradio08/playlist.m3u8",""));
        radioList.add(new TVBean(9, R.string.radio_jingji96, R.drawable.cover_jingji96, radioUrl + "sdradio09/playlist.m3u8",""));
    }

    public static ArrayList<TVBean> getTvList() {
        return tvList;
    }

    public static ArrayList<TVBean> getRadioList() {
        return radioList;
    }

    // 获取山东卫视的信息
    public static TVBean getWeiShi() {
        return new TVBean(24, R.string.tv_weishi, R.drawable.cover_ws, tvUrl + "sdtv.m3u8", "/live2/sdtv.m3u8");
    }

    // 获取新闻频道的信息
    public static TVBean getNews() {
        return new TVBean(1, R.string.radio_news, R.drawable.cover_renmintai, radioUrl + "sdradio01/playlist.m3u8", "");
    }

    public static TVBean getTvById(int id){
        TVBean result = null;
        for (TVBean bean : tvList) {
            if (bean.getId() == id) {
                result = bean;
                break;
            }
        }
        return result;
    }

    public static TVBean getRadioById(int id) {
        TVBean result = null;
        for (TVBean bean : radioList) {
            if (bean.getId() == id) {
                result = bean;
                break;
            }
        }
        return result;
    }
}
