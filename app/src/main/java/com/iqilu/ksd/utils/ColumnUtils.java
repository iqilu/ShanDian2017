package com.iqilu.ksd.utils;

import com.iqilu.ksd.R;
import com.iqilu.ksd.bean.ColumnItemBean;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/10/11.
 */

public class ColumnUtils {

    public static ArrayList<ColumnItemBean> columnList;

    static {
        columnList = new ArrayList<ColumnItemBean>();
        columnList.add(new ColumnItemBean(0, "头条", R.drawable.menu_icon_toutiao));
        columnList.add(new ColumnItemBean(0, "订阅", R.drawable.menu_icon_dingyue));
        columnList.add(new ColumnItemBean(0, "济南", R.drawable.menu_icon_bendi));
        columnList.add(new ColumnItemBean(57, "时政", R.drawable.menu_icon_shizheng));
        columnList.add(new ColumnItemBean(145,"环球",R.drawable.menu_icon_huanqiu));
        columnList.add(new ColumnItemBean(77, "评论", R.drawable.menu_icon_pinglun));
        columnList.add(new ColumnItemBean(63, "影像", R.drawable.menu_icon_yingxiang));
        columnList.add(new ColumnItemBean(163, "旅游", R.drawable.menu_icon_lvyou));
        columnList.add(new ColumnItemBean(66, "闪电体育", R.drawable.menu_icon_tiyu));
        columnList.add(new ColumnItemBean(5, "闪电娱乐", R.drawable.menu_icon_yule));
//        columnList.add(new ColumnItemBean(62, "深度", R.drawable.menu_icon_shendu));
//        columnList.add(new ColumnItemBean(61, "财经", R.drawable.menu_icon_caijing));
//        columnList.add(new ColumnItemBean(65, "民生", R.drawable.menu_icon_minsheng));
//        columnList.add(new ColumnItemBean(69, "教育", R.drawable.menu_icon_jiaoyu));
//        columnList.add(new ColumnItemBean(5, "食品", R.drawable.menu_icon_shipin));
//        columnList.add(new ColumnItemBean(28, "自媒体", R.drawable.menu_icon_zimeiti));
    }

    public static ArrayList<ColumnItemBean> getColumnList() {
        return columnList;
    }
}
