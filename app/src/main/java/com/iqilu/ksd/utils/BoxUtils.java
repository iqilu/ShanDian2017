package com.iqilu.ksd.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.ADActivity;
import com.iqilu.ksd.activity.MyActivity;
import com.iqilu.ksd.activity.ShakeActivity;
import com.iqilu.ksd.activity.ShakeListActivity;
import com.iqilu.ksd.activity.VoteSignActivity;
import com.iqilu.ksd.constant.Config;
import com.iqilu.ksd.constant.VoteSign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;

/**
 * 触电
 * Created by Coofee on 2016/12/29.
 */

public class BoxUtils {

    private static BoxUtils instance = null;

    private Context context;
    private AlertDialog dialog;
    private GridView gvList;
    private RelativeLayout layCancel;
    private SimpleAdapter adapter;
    private int[] images = {R.drawable.box_shake, R.drawable.box_tp, R.drawable.box_bm, R.drawable.box_mall};

    private static final String URL_MALL = "https://h5.youzan.com/v2/showcase/feature?alias=n4w6prjs&dc_ps=11934";

    private BoxUtils() {
    }

    public static BoxUtils getInstance() {
        if (instance == null) {
            instance = new BoxUtils();
        }
        return instance;
    }

    public BoxUtils init(Context context) {
        this.context = context;
        return this;
    }

    public void show() {
        dialog = new AlertDialog.Builder(context).create();
        dialog.show();
        Window window = dialog.getWindow();
        window.setContentView(R.layout.popu_box);
        gvList = (GridView) window.findViewById(R.id.gv_list);
        layCancel = (RelativeLayout) window.findViewById(R.id.lay_cancel);
        List<HashMap<String, Object>> list = new ArrayList<HashMap<String, Object>>();
        for (int i = 0; i < images.length; i++) {
            HashMap<String, Object> map = new HashMap<String, Object>();
            map.put("img_thumb", images[i]);
            list.add(map);
        }
        adapter = new SimpleAdapter(context, list, R.layout.list_item_share, new String[]{"img_thumb"}, new int[]{R.id.img_thumb});
        layCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        gvList.setAdapter(adapter);
        gvList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        context.startActivity(new Intent(context, ShakeListActivity.class));
                        break;
                    case 1:
                        Intent voteIntent = new Intent(context, VoteSignActivity.class);
                        voteIntent.putExtra("type", VoteSign.VOTE);
                        context.startActivity(voteIntent);
                        break;
                    case 2:
                        Intent signIntent = new Intent(context, VoteSignActivity.class);
                        signIntent.putExtra("type", VoteSign.SIGN);
                        context.startActivity(signIntent);
                        break;
                    case 3:
                        Intent mallIntent = new Intent(context, ADActivity.class);
                        mallIntent.putExtra("adUrl", URL_MALL);
                        context.startActivity(mallIntent);
                        break;
//                    case 4:
//                        context.startActivity(new Intent(context, MyActivity.class));
                    default:
                        break;
                }
                dialog.dismiss();
            }
        });

    }


}
