package com.iqilu.ksd.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by Coofee on 2016/10/11.
 */

public class SPUtils {
    /**
     * 获取偏好值
     */
    public static boolean getPref(Context context, String key, Boolean defValue) {
        boolean value = defValue;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            value = prefs.getBoolean(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取偏好值
     */
    public static int getPref(Context context, String key, int defValue) {
        int value = defValue;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            value = prefs.getInt(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取偏好值
     */
    public static float getPref(Context context, String key, float defValue) {
        float value = defValue;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            value = prefs.getFloat(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public static long getPrefLong(Context context, String key, long defValue) {
        long value = defValue;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            value = prefs.getLong(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 获取偏好值
     */
    public static String getPref(Context context, String key, String defValue) {
        String value = defValue;
        try {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
            value = prefs.getString(key, defValue);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 设置偏好值
     */
    public static void setPref(Context context, String key, Boolean value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putBoolean(key, value);
        edit.commit();
    }

    /**
     * 设置偏好值
     */
    public static void setPref(Context context, String key, int value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putInt(key, value);
        edit.commit();
    }

    public static void setPrefForLong(Context context, String key, long value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putLong(key, value);
        edit.commit();
    }

    /**
     * 设置偏好值
     */
    public static void setPref(Context context, String key, float value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putFloat(key, value);
        edit.commit();
    }

    /**
     * 设置偏好值
     */
    public static void setPref(Context context, String key, String value) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    /**
     * 移除偏好值
     *
     * @param context
     * @param key
     */
    public static void delPref(Context context, String key) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = prefs.edit();
        edit.remove(key);
        edit.commit();
    }
}
