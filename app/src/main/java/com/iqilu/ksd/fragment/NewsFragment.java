package com.iqilu.ksd.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.NewsFragmentPagerAdapter;
import com.iqilu.ksd.bean.ColumnItemBean;
import com.iqilu.ksd.utils.ColumnUtils;
import com.iqilu.ksd.widget.MyViewPager;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/11/18.
 */

public class NewsFragment extends BaseFragment {

    private ArrayList<ColumnItemBean> columnList;
    private ArrayList<Fragment> fragments;

    private MyViewPager vpContent;
    private RelativeLayout layLoading;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_news,null);
        vpContent = getView(view,R.id.vp_content);
        layLoading = getView(view,R.id.lay_loading);
        initFragment();
        return view;
    }

    /**
     * 初始化Fragment
     */
    private void initFragment() {
        columnList = ColumnUtils.getColumnList();
        fragments = new ArrayList<Fragment>();
        TopFragment topfragment = new TopFragment();
        SubsFragment2 subsfragment = new SubsFragment2();
        CityFragment cityfragment = new CityFragment();
        fragments.add(topfragment);
        fragments.add(subsfragment);
        fragments.add(cityfragment);
        for (int i = 3; i < columnList.size(); i++) {
            Bundle data = new Bundle();
            data.putInt("catId", columnList.get(i).getId());
            data.putString("title",columnList.get(i).getName());
            CommonFragment commonfragment = new CommonFragment();
            commonfragment.setArguments(data);
            fragments.add(commonfragment);
        }
        NewsFragmentPagerAdapter pagerAdapter = new NewsFragmentPagerAdapter(getChildFragmentManager(), fragments, columnList);
        vpContent.setAdapter(pagerAdapter);
    }

    public void setCurrentItem(int position){
        vpContent.setCurrentItem(position);
    }
}
