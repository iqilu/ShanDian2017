package com.iqilu.ksd.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.iqilu.ksd.bean.UserBean;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Coofee on 2016/10/11.
 */

public class BaseFragment extends Fragment {

    public Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void setZHTypeface(TextView tv) {
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTZHUNHK.TTF");
        tv.setTypeface(typeFace);
    }

    public void setXHTypeface(TextView tv) {
        Typeface typeFace = Typeface.createFromAsset(context.getAssets(), "fonts/FZLTXHK.TTF");
        tv.setTypeface(typeFace);
    }

    public <T extends View> T getView(View view, int id) {
        return (T) view.findViewById(id);
    }
}
