package com.iqilu.ksd.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.TVFragmentPagerAdapter;

import java.util.ArrayList;

/**
 * Created by Coofee on 2016/10/11.
 */

public class TVFragment extends BaseFragment {

    private String[] mTitles;
    private TVListFragment tvListFragment;
    private GuideFragment guideFragment;
    private ArrayList<Fragment> fragmentList;

    private RelativeLayout layHead;
    private SegmentTabLayout mTab;
    private ViewPager mViewPager;
    private TVFragmentPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv, null);
        layHead = getView(view,R.id.lay_head);
        mTab = getView(view,R.id.tab_menu);
        mTitles = new String[]{getString(R.string.live), getString(R.string.dianbo)};
        mViewPager = getView(view,R.id.vp_content);
        init();
        return view;
    }

    private void init() {
        tvListFragment = new TVListFragment();
        guideFragment = new GuideFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "tv");
        guideFragment.setArguments(bundle);
        fragmentList = new ArrayList<>();
        fragmentList.add(tvListFragment);
        fragmentList.add(guideFragment);
        adapter = new TVFragmentPagerAdapter(getChildFragmentManager(), fragmentList);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTab.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTab.setTabData(mTitles);
        mTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mViewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    public void toggleHide(int status){
        layHead.setVisibility(status);
    }

    public void resetPageToPortrait() {
        if (tvListFragment != null) {
            tvListFragment.resetPageToPortrait();
        }
    }

}
