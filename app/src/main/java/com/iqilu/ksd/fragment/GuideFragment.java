package com.iqilu.ksd.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.gson.reflect.TypeToken;
import com.iqilu.ksd.R;
import com.iqilu.ksd.activity.RadioDetailActivity;
import com.iqilu.ksd.activity.VideoDetailActivity;
import com.iqilu.ksd.adapter.GuideChildAdapter;
import com.iqilu.ksd.adapter.GuideParentAdapter;
import com.iqilu.ksd.bean.GuideBean;
import com.iqilu.ksd.bean.GuideChildBean;
import com.iqilu.ksd.constant.Api;
import com.iqilu.ksd.event.StopPlayRadioEvent;
import com.iqilu.ksd.event.StopPlayVideoEvent;
import com.iqilu.ksd.utils.JSONUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * 节目单
 * Created by Coofee on 2016/11/16.
 */

public class GuideFragment extends BaseFragment {

    private static final String TAG = "GuideFragment";

    private int id = 0;
    private String type;
    private ListView lvParent;
    private ListView lvChild;

    private ImageView imgEmpty;
    private RelativeLayout layParent;
    private RelativeLayout layLoading;
    private GuideParentAdapter parentAdapter;
    private GuideChildAdapter childAdapter;
    private ArrayList<GuideChildBean> childList;
    private ArrayList<GuideBean> parentList;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_guide, null);
        Bundle bundle = getArguments();
        type = bundle.getString("type");
        lvParent = getView(view, R.id.lv_parent);
        lvChild = getView(view, R.id.lv_child);
        layLoading = getView(view, R.id.lay_loading);
        imgEmpty = getView(view, R.id.img_empty);
        layParent = getView(view,R.id.lay_parent);
        parentAdapter = new GuideParentAdapter(context);
        childAdapter = new GuideChildAdapter(context, type);
        lvParent.setAdapter(parentAdapter);
        lvChild.setAdapter(childAdapter);
        lvChild.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                GuideChildBean bean = childList.get(position);
                Intent intent;
                if(type.equals("tv")){
                    intent = new Intent(context, VideoDetailActivity.class);
                    EventBus.getDefault().post(new StopPlayVideoEvent());
                }else {
                    intent = new Intent(context, RadioDetailActivity.class);
                    EventBus.getDefault().post(new StopPlayRadioEvent());
                }
                intent.putExtra("catid",bean.getCatid());
                startActivity(intent);
            }
        });
        lvParent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parentAdapter.setCurId(parentList.get(position).getCatid());
                parentAdapter.notifyDataSetChanged();
                childList = parentList.get(position).getCates();
                childAdapter.setData(childList);
                childAdapter.notifyDataSetChanged();
            }
        });
        getList();
        return view;
    }

    private void getList() {
        String url;
        if (type.equals("tv")) {
            url = Api.URL_CATE_TV;
        } else {
            url = Api.URL_CATE_RADIO;
        }
        Log.i(TAG, "url=" + url);
        OkHttpUtils.get().url(url).tag(this).build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                layParent.setVisibility(View.GONE);
                layLoading.setVisibility(View.GONE);
                imgEmpty.setVisibility(View.VISIBLE);
            }

            @Override
            public void onResponse(String response, int id) {
                layLoading.setVisibility(View.GONE);
                parentList = JSONUtils.requestList(response, "data", new TypeToken<ArrayList<GuideBean>>() {
                });
                if (parentList != null && parentList.size() > 0) {
                    parentAdapter.setData(parentList);
                    parentAdapter.setCurId(parentList.get(0).getCatid());
                    parentAdapter.notifyDataSetChanged();
                    childList = parentList.get(0).getCates();
                    childAdapter.setData(childList);
                    childAdapter.notifyDataSetChanged();
                } else {
                    layParent.setVisibility(View.GONE);
                    layLoading.setVisibility(View.GONE);
                    imgEmpty.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
