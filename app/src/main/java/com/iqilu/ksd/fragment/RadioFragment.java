package com.iqilu.ksd.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.iqilu.ksd.R;
import com.iqilu.ksd.adapter.TVFragmentPagerAdapter;

import java.util.ArrayList;

/**
 * 听广播
 * Created by Coofee on 2017/1/18.
 */

public class RadioFragment extends BaseFragment {

    private String[] mTitles;
    private GuideFragment guideFragment;
    private RadioListFragment radioListFragment;
    private ArrayList<Fragment> fragmentList;

    private RelativeLayout layHead;
    private SegmentTabLayout mTab;
    private ViewPager mViewPager;
    private TVFragmentPagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tv, null);
        layHead = getView(view, R.id.lay_head);
        mTab = getView(view, R.id.tab_menu);
        mTitles = new String[]{getString(R.string.live), getString(R.string.dianbo)};
        mViewPager = getView(view, R.id.vp_content);
        init();
        return view;
    }

    private void init() {
        radioListFragment = new RadioListFragment();
        guideFragment = new GuideFragment();
        Bundle bundle = new Bundle();
        bundle.putString("type", "radio");
        guideFragment.setArguments(bundle);
        fragmentList = new ArrayList<>();
        fragmentList.add(radioListFragment);
        fragmentList.add(guideFragment);
        adapter = new TVFragmentPagerAdapter(getChildFragmentManager(), fragmentList);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mTab.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        mTab.setTabData(mTitles);
        mTab.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                mViewPager.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }
}
