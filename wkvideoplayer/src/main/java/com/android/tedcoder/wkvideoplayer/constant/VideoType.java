package com.android.tedcoder.wkvideoplayer.constant;

/**
 * 视频、音频类型
 * Created by coofee on 2015/12/5.
 */
public class VideoType {

    public static int VIDEO_LIVE = 1; // 视频在线直播

    public static int VIDEO_ONLINE = 2; // 视频在线点播

    public static int RADIO_LIVE = 3; // 音频在线直播

    public static int RADIO_ONLINE = 4; // 音频在线点播
}
