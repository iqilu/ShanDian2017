package com.android.tedcoder.wkvideoplayer.util;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;

import java.security.MessageDigest;
import java.util.Arrays;

/**
 * 直播地址加密工具类
 * <p/>
 * Created by coofee on 2016/1/15.
 */
public class EncryptUtil {

    private static final String liveKey = "RZp22OCXRbWXkUYbgpHM"; //直播key

    private static final String AESKey = "a4deadfdb70e2a0e"; //AES加密key

    private static final String ivKey = "d0d72712de3ba0c1";

    public static String getLiveToken(String path, String deviceId, String expireTime) {
        byte[] md5Byte;
        String token = "";
        md5Byte = BaseTool.md5(liveKey + path + expireTime + deviceId);
        token = Base64.encodeToString(md5Byte, Base64.DEFAULT);
        return replaceStr(token);
    }

    public static String getHeartKey(String token, String time) {
        String text = "" + Math.random() + "*" + token + "*" + time + "*" + Math.random() + "*" + "android";
        byte[] encryptTextByte = BaseTool.AESEncrypt(text, AESKey, ivKey);
        String encryptText = Base64.encodeToString(encryptTextByte, Base64.DEFAULT);
//        return encryptText;
        return replaceStr(encryptText);
    }

    private static String replaceStr(String str) {
        String repStr = "";
        repStr = str.replace('+', '-');
        repStr = repStr.replace('/', '_');
        repStr = repStr.replaceAll("=", "");
        repStr = repStr.replaceAll("\r", "");
        repStr = repStr.replaceAll("\n", "");
        return repStr;
    }


}
