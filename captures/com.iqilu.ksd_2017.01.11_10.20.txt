Applications Memory Usage (kB):
Uptime: 49406712 Realtime: 89005760

** MEMINFO in pid 7875 [com.iqilu.ksd] **
                   Pss  Private  Private  Swapped     Heap     Heap     Heap
                 Total    Dirty    Clean    Dirty     Size    Alloc     Free
                ------   ------   ------   ------   ------   ------   ------
  Native Heap    68607    68576        0    12948    94464    91649     2814
  Dalvik Heap    48170    48108        0     9576    65009    61666     3343
 Dalvik Other     2387     2384        0        0                           
        Stack      936      936        0        0                           
       Ashmem     5530     4012        0        0                           
    Other dev        9        0        8        0                           
     .so mmap     3475      240      684     2128                           
    .apk mmap    26284      100    17824        0                           
    .ttf mmap     1088        0      676        0                           
    .dex mmap    13502      304     9480        0                           
    .oat mmap     4427        0     1368        4                           
    .art mmap     2136     1640       20       88                           
   Other mmap      920        8      764        0                           
    GL mtrack   130392   130392        0        0                           
      Unknown     6204     6204        0       80                           
        TOTAL   314067   262904    30824    24824   159473   153315     6157
 
 App Summary
                       Pss(KB)
                        ------
           Java Heap:    49768
         Native Heap:    68576
                Code:    30676
               Stack:      936
            Graphics:   130392
       Private Other:    13380
              System:    20339
 
               TOTAL:   314067      TOTAL SWAP (KB):    24824
 
 Objects
               Views:      377         ViewRootImpl:        3
         AppContexts:        6           Activities:        4
              Assets:        9        AssetManagers:        2
       Local Binders:       42        Proxy Binders:       39
       Parcel memory:       13         Parcel count:       57
    Death Recipients:        2      OpenSSL Sockets:        1
 
 SQL
         MEMORY_USED:        1
  PAGECACHE_OVERFLOW:        0          MALLOC_SIZE:       62
 
 
 Asset Allocations
    zip:/data/app/com.iqilu.ksd-2/base.apk:/assets/fonts/FZLTXHK.TTF: 7544K
    zip:/data/app/com.iqilu.ksd-2/base.apk:/assets/fonts/FZLTZHUNHK.TTF: 7510K
    zip:/data/app/com.iqilu.ksd-2/base.apk:/assets/fonts/FZLTZHUNHK.TTF: 7510K
    zip:/data/app/com.iqilu.ksd-2/base.apk:/assets/fonts/FZLTZHUNHK.TTF: 7510K
    zip:/data/app/com.iqilu.ksd-2/base.apk:/assets/fonts/FZLTZHUNHK.TTF: 7510K
